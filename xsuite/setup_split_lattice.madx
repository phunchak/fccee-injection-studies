
!----------------------------------------------------------------------------------------
! Set up Lattice
!----------------------------------------------------------------------------------------

SET, FORMAT = "19.15f";

CALL, FILE = "/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/sequences/FCCee_z_216_nosol_1.seq";

!----------------------------------------------------------------------------------------
! Define Beam
!----------------------------------------------------------------------------------------
! found in fcc_ee_z.madx on https://gitlab.cern.ch/mihofer/fcc-ee-collimation-lattice/-/blob/master/tests/fcc_ee_z.madx

pbeam := 45.6;      !beam 
EXbeam = 0.27e-9;   !horizontal emittance
EYbeam = 1.0e-12;   !vertical emittance
Nbun := 16640;      !number of bunches
NPar := 1.7e11;     !number of particles per bunch

Ebeam := sqrt( pbeam^2 + emass^2 ); !energy of the beam


// Beam defined without radiation as a start - radiation is turned on later depending on the requirements
BEAM, PARTICLE=POSITRON, NPART=Npar, KBUNCH=Nbun, ENERGY=Ebeam, RADIATE=FALSE, BV=+1, EX=EXbeam, EY=EYbeam;

USE, SEQUENCE = L000015;
SAVEBETA, LABEL=B.RF1, PLACE=FF2.1, SEQUENCE=L000015;
SAVEBETA, LABEL=B.RF2, PLACE=FF2.3, SEQUENCE=L000015;
TWISS, TOLERANCE=1E-12; ! Twiss without radiation and tapering

!----------------------------------------------------------------------------------------
! Perform initial Twiss and survey - ideal machine with no radiation
!----------------------------------------------------------------------------------------

SEQEDIT, SEQUENCE = L000015;
         FLATTEN;
         INSTALL, ELEMENT = INJSEPTMARK, CLASS = MARKER, AT = 5922.736613;
         FLATTEN;
ENDEDIT;


CALL, FILE="/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/scripts/test_nlk_python_implementation/install_kicker_markers.madx";

!----------------------------------------------------------------------------------------
! Load Aperture definitions
!----------------------------------------------------------------------------------------
CALL, FILE="/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/sequences/aperture/FCCee_aper_definitions.madx";
CALL, FILE="/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/sequences/install_collimators.madx";

!----------------------------------------------------------------------------------------
!Install Orbit Correctors and BPMs
!----------------------------------------------------------------------------------------
!CALL, FILE = /afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/sequences/install_correctors.madx;

// Save the voltage settings for the cavities for later use if needed
VOLTCA1SAVE = VOLTCA1; 

SHOW, VOLTCA1SAVE;

// Turn off the cavities for ideal machine twiss and survey
VOLTCA1 = 0;

SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L,K0L,K1L,K2L,K3L,K4L,K1SL,K2SL,K3SL,K4SL,HKICK,VKICK,BETX,BETY,ALFX,ALFY,MUX,MUY,DX,DY,DPX,DPY,R11,R12,R22,R21,X,PX,Y,PY,T,PT,DELTAP,VOLT,LAG,HARMON,FREQ,E1,E2,APERTYPE,APER_1,APER_2,APER_3,APER_4,TILT,ANGLE;

// Place where the initial conditions are saved - used for RF matching later if needed
SAVEBETA, LABEL=B.IP, PLACE=#s, SEQUENCE=L000015;

TWISS;

!---------------------------------------------------------------------------------------
!Turn on RF and Synchrotron Radiation
!----------------------------------------------------------------------------------------

// RF back on
VOLTCA1 = VOLTCA1SAVE;

// Turn the beam radiation on. N.B. This simple toggle works only if the sequence is not defined in the orginal beam command.
BEAM, RADIATE=TRUE;

// RF matching
LAGCA1 = 0.5;

MATCH, sequence=L000015, BETA0 = B.IP, tapering;
  VARY, NAME=LAGCA1, step=1.0E-7;
  CONSTRAINT, SEQUENCE=L000015, RANGE=#e, PT=0.0;
  JACOBIAN,TOLERANCE=1.0E-14, CALLS=3000;
ENDMATCH;

// Twiss with tapering
USE, SEQUENCE = L000015;

SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L,K0L,K1L,K2L,K3L,K4L,K1SL,K2SL,K3SL,K4SL,HKICK,VKICK,BETX,BETY,ALFX,ALFY,MUX,MUY,DX,DY,DPX,DPY,R11,R12,R22,R21,X,PX,Y,PY,T,PT,DELTAP,VOLT,LAG,HARMON,FREQ,E1,E2,APERTYPE,APER_1,APER_2,APER_3,APER_4,TILT,ANGLE;

TWISS, TAPERING;

!-------------------------------------------------------------------------------
! Slice the lattice and save a thin sequence
!-------------------------------------------------------------------------------
// Note: if the tapering was enabled in the previous steps, MAKETHIN will inherit the
// tapered magnetic strenghts (KNTAP) calculated by TWISS and the element strengths (KN) accordingly to produce a tapered thin sequence.
// If any of: RF, beam radiation, tapering; were disabled before, the resulting thin sequence is not tapered.

// Slicing with special attention to IR quads and sextupoles     
SELECT, FLAG=makethin, CLASS=RFCAVITY, SLICE = 1;
SELECT, FLAG=makethin, CLASS=rbend, SLICE = 8;		!originally 4 slices
SELECT, FLAG=makethin, CLASS=quadrupole, SLICE = 6;	!originally 4 slices
SELECT, FLAG=makethin, CLASS=sextupole, SLICE = 6;	!originally 4 slices
        
SELECT, FLAG=makethin, PATTERN="^QC*", SLICE=20;
SELECT, FLAG=makethin, PATTERN="^SY*", SLICE=20;
        
MAKETHIN, SEQUENCE=L000015 STYLE=TEAPOT, MAKEDIPEDGE=false;

USE, SEQUENCE = L000015;

SAVEBETA, LABEL=B.IP1, PLACE=IP.1, SEQUENCE=L000015;
SAVEBETA, LABEL=B.MKI, PLACE=MKI_MARKER, SEQUENCE=L000015;
SAVEBETA, LABEL=B.IP2, PLACE=IP.2, SEQUENCE=L000015;
TWISS;


!-------------------------------------------------------------------------------
!Split lattice for use in XSuite
!-------------------------------------------------------------------------------
SEQEDIT, SEQUENCE = L000015;
	FLATTEN;
	EXTRACT, SEQUENCE=L000015, FROM=IP.1,	TO=MKI_MARKER,	NEWNAME=IP1_TO_MKI_L000015;	
	EXTRACT, SEQUENCE=L000015, FROM=MKI_MARKER,TO=IP.2,	NEWNAME=MKI_TO_IP2_L000015;	
	EXTRACT, SEQUENCE=L000015, FROM=IP.2,	TO=IP.4,	NEWNAME=IP2_TO_IP4_L000015;
ENDEDIT;

SAVE, SEQUENCE=IP1_TO_MKI_L000015,FILE = IP1toMKI.seq;
SAVE, SEQUENCE=MKI_TO_IP2_L000015,FILE = MKItoIP2.seq;
SAVE, SEQUENCE=IP2_TO_IP4_L000015,FILE = IP2toIP4.seq;

USE, SEQUENCE=IP1_TO_MKI_L000015;
TWISS, BETA0=B.IP1, FILE="twiss_IP1_to_MKI.tfs";

USE, SEQUENCE=MKI_TO_IP2_L000015;
TWISS, BETA0=B.MKI, FILE="twiss_MKI_to_IP2.tfs";

USE, SEQUENCE=IP2_TO_IP4_L000015;
TWISS, BETA0=B.IP2, FILE="twiss_IP2_to_IP4.tfs";






