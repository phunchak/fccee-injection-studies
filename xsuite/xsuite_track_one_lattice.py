import numpy as np
import xobjects as xo
import xtrack as xt
import xpart as xp
import xfields as xf
from cpymad.madx import Madx
import matplotlib.pyplot as plt
import pandas as pd
import tfs
from scipy.constants import e as qe
from scipy.constants import m_p
import math
from nlk_tracking import apply_kick

##Load Sequence with apertures
#mad = Madx()
#mad.input("BEAM;")
#mad.call("full_2ip_z_w_apertures.seq")
#mad.use("L000015")
#
#line1= xt.Line.from_madx_sequence(mad.sequence['L000015'], apply_madx_errors=False, install_apertures=True)

## Choose a context
context = xo.ContextCpu()         # For CPU
# context = xo.ContextCupy()      # For CUDA GPUs
# context = xo.ContextPyopencl()  # For OpenCL GPUs

##stored beam parameters for FCCee Z-mode (not all values updated from ttbar settings in example)

bunch_intensity     = 1.7e11  # [1]
energy              = 45.6  # [GeV]
p0c                 = 45.6e9  # [eV]
mass0               = .511e6  # [eV]
phi                 = 15e-3  # [rad] half xing
#u_sr                = 9.2  # [GeV]
#u_bs                = .0114  # [GeV]
#k2_factor           = .4  # [1]
#qx                  = .554  # [1] half arc
#qy                  = .588  # [1]
#qs                  = .0436  # [1]
stor_physemit_x          = 0.27e-09  # [m]
stor_physemit_y          = 1e-12  # [m]
beta_x_ip           = 0.15  # [m]
beta_y_ip           = 0.00078  # [m]
beta_x_sept         = 1981.428866  # [m]
beta_y_sept         = 312.89228  # [m]
beta_x_kicker          = 193.036  # [m]
beta_y_kicker          = 51.252  # [m]
sigma_x_stor_ip     = np.sqrt(stor_physemit_x*beta_x_ip)  # [m]
sigma_px_stor_ip    = np.sqrt(stor_physemit_x/beta_x_ip)  # [m]
sigma_y_stor_ip     = np.sqrt(stor_physemit_y*beta_y_ip)  # [m]
sigma_py_stor_ip    = np.sqrt(stor_physemit_y/beta_y_ip)  # [m]
sigma_x_stor_sept   = np.sqrt(stor_physemit_x*beta_x_sept)  # [m]
sigma_px_stor_sept  = np.sqrt(stor_physemit_x/beta_x_sept)  # [m]
sigma_y_stor_sept   = np.sqrt(stor_physemit_y*beta_y_sept)  # [m]
sigma_py_stor_sept  = np.sqrt(stor_physemit_y/beta_y_sept)  # [m]
sigma_x_stor_kicker    = np.sqrt(stor_physemit_x*beta_x_kicker)  # [m]
sigma_px_stor_kicker   = np.sqrt(stor_physemit_x/beta_x_kicker)  # [m]
sigma_y_stor_kicker    = np.sqrt(stor_physemit_y*beta_y_kicker)  # [m]
sigma_py_stor_kicker   = np.sqrt(stor_physemit_y/beta_y_kicker)  # [m]
#sigma_z             = .00194  # [m] sr
sigma_z             = 0
#sigma_z_tot         = .00254  # [m] sr+bs
#sigma_delta         = .0015  # [m]
sigma_delta         = 0
#sigma_delta_tot     = .00192  # [m]
#beta_s              = sigma_z/sigma_delta  # [m]
#physemit_s          = sigma_z*sigma_delta  # [m]
#physemit_s_tot      = sigma_z_tot*sigma_delta_tot  # [m]
nparticles_stored   = 10000
#n_macroparticles_b1 = int(1e3)
#n_macroparticles_b2 = int(1e6)

#10000 particles in weak beam 1e6 in strong beam

##Define Beam-beam element
el_beambeam_b1 = xf.BeamBeamBiGaussian2D(
        _context=context,
        config_for_update = None,
        other_beam_q0=1,
        other_beam_beta0=1,
        other_beam_num_particles=1e7,
        other_beam_Sigma_11=sigma_x_stor_ip,
        other_beam_Sigma_13=None,
        other_beam_Sigma_33=sigma_y_stor_ip,
        # decide between round or elliptical kick formula
        min_sigma_diff     = 1e-28,       
        
)

##Install beambeam elements at IPs
line1.insert_element(at_s=91433.864292938058497, element=el_beambeam_b1, name="IP1")
line1.insert_element(at_s=42554.432146472900058, element=el_beambeam_b1, name="IP2")

## Transfer lattice on context and compile tracking code
l000015 = xt.Tracker(_context=context, line=line1)
## Turn on radiation for each of the tracking segments
l000015.configure_radiation(mode='mean')

##Initialize stored beam at MKI

df_stored_kicker = pd.DataFrame(index=range(nparticles_stored), columns=["X","PX","Y","PY"])
df_stored_kicker["X"] = sigma_x_stor_kicker * np.random.randn(nparticles_stored)
df_stored_kicker["PX"]= sigma_px_stor_kicker * np.random.randn(nparticles_stored)
df_stored_kicker["Y"] = sigma_y_stor_kicker * np.random.randn(nparticles_stored)
df_stored_kicker["PY"]= sigma_py_stor_kicker * np.random.randn(nparticles_stored)

##injected beam parameters for FCCee Z-mode (not all values updated from ttbar settings in example)
fraction_injected_beam = 0.1
bunch_intensity     = 1.7e11*fraction_injected_beam  # [1]
energy              = 45.6  # [GeV]
p0c                 = 45.6e9  # [eV]
mass0               = .511e6  # [eV]
phi                 = 15e-3  # [rad] half xing
inj_physemit_x      = 0.27e-09  # [m]
inj_physemit_y      = 1e-12  # [m]
sigma_x_inj         = np.sqrt(inj_physemit_x*beta_x_kicker)  # [m]
sigma_px_inj        = np.sqrt(inj_physemit_x/beta_x_kicker)  # [m]
sigma_y_inj         = np.sqrt(inj_physemit_y*beta_y_kicker)  # [m]
sigma_py_inj        = np.sqrt(inj_physemit_y/beta_y_kicker)  # [m]
sigma_z             = 0
sigma_delta         = 0
nparticles_injected = int(nparticles_stored*fraction_injected_beam)

## Is selecting of mode needed? or is selection of initial phase sufficient

##Inialize injected beam at SEPTUM
orbitbump_inj_flag = 1 #flag to either include or ignore the separation from the septum
septumwidth=3e-3
df_inj 	    = pd.DataFrame(index=range(nparticles_injected), columns=["X","PX","Y","PY"])
stor_beam_sigmas = 5 # size on stored beam in sigmas
inj_beam_sigmas  = 5 # size of injected beam in sigmas
optional_sigma_shift = 0 # number of (stored beam) sigmas to offset the injected beam beyond what is absolutely necessary
phase_at_injection = 20

#(which is not a factor in mki injected beam positioning after the kick)
inj_radial_offsetx = (((stor_beam_sigmas + orbitbump_inj_flag*(septumwidth / sigma_x_stor_sept) ) * sigma_x_stor_sept)\
 	+ (inj_beam_sigmas * sigma_x_inj) \
 	+ (optional_sigma_shift) * sigma_x_stor_sept )

inj_x = inj_radial_offsetx * np.cos(phase_at_injection*np.pi/180)

inj_px = inj_radial_offsetx * np.sin(phase_at_injection*np.pi/180)

df_inj["X"] = inj_x  + sigma_x_inj  * np.random.randn(nparticles_injected)
df_inj["PX"]= inj_px + sigma_px_inj * np.random.randn(nparticles_injected)
df_inj["Y"] = sigma_y_inj  * np.random.randn(nparticles_injected)
df_inj["PY"]= sigma_py_inj * np.random.randn(nparticles_injected)


#Transport Injected Beam from Septum to kicker and apply kick.
def pure_phase_rot(bet1,bet2,alpha1,alpha2,deltamu,pos1,ang1):
	#bet1,alpha1: twiss parameters at start point
	#bet2,alpha2: twiss parameters at end point 
	#deltamu: pure rotation of the phase from start to end point (radians)
	#pos1: positional coordinate
	#ang1: angle coordinate
	M = np.array([[(np.sqrt(bet2/bet1)*(np.cos(deltamu)+alpha1*np.sin(deltamu))),(np.sqrt(bet1*bet2)*np.sin(deltamu))],\
		[(((alpha1-alpha2)/np.sqrt(bet1*bet2))*np.cos(deltamu) - ((1+alpha1*alpha2)/(np.sqrt(bet1*bet2))*np.sin(deltamu))),(np.sqrt(bet1/bet2)*(np.cos(deltamu) - alpha2*np.sin(deltamu)))]])
	pos2 = np.zeros_like(pos1)
	ang2 = np.zeros_like(ang1)
	for i,(x,p) in enumerate(zip(pos1,ang1)):
		pos2[i],ang2[i] = M@np.array([x,p])
	
	return pos2,ang2

#90 deg phase rotation
pos_after,ang_after = pure_phase_rot(1,1,1,1,0.5*math.pi,df_inj["X"].to_numpy(),df_inj["PX"].to_numpy())
#Apply MKI kick
coords = apply_kick(coordinates = pd.DataFrame(data={"X":pos_after,"PX":ang_after}),
	kickfile = "/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/kicker_data/KickerFieldProfile.csv",
        kickstrength    = (0.5/152.105227)*(-0.0079), #0.5/beam_rigidity * kick_factor
	conversion = {'X':1E-3, 'BY':1E-4},
	columns = {'X':0, 'BY':1},
        x_inj = 0.00575, #could update to be based on injected beam actual position
        offset      = 0)



#plot initial distribution and after rotation
fig,ax = plt.subplots(nrows=1,ncols=1,figsize=(9,9))

ax.scatter(df_inj["X"],df_inj["PX"])
ax.scatter(pos_after,ang_after)
ax.scatter(coords["X"],coords["PX_afterkick"])
#ax.set_xlim([-2e-2,2e-2])
#ax.set_ylim([-1e-2,1e-2])
plt.show()

##Combine stored and injected beams into one if desired
#df_stor_and_inj = pd.concat([df_stored_mki,df_inj],axis=0,ignore_index=True)




#end file for debugging purposes
exit()

##Create particles object (currently just injected beam)
particles = xp.Particles(
            _context = context, 
            q0        = -1,
            p0c       = p0c,
            mass0     = mass0,
            x         = df_inj["X"],
            y         = df_inj["Y"],
            zeta      = np.zeros(len(df_inj["X"])),
            px        = df_inj["PX"],
            py        = df_inj["PY"],
            delta     = np.zeros(len(df_inj["X"])),
            )

##Track Particles object through n_turns
n_turns=3*2500
l000015.track(particles, num_turns=n_turns, turn_by_turn_monitor=True)

##Save turn by turn horizontal distribution to text files
np.savetxt('Outputdata/xdata.csv', l000015.record_last_track.x,  fmt='%f', delimiter=",")
np.savetxt('Outputdata/pxdata.csv',l000015.record_last_track.px, fmt='%f', delimiter=",")
np.savetxt('Outputdata/ydata.csv', l000015.record_last_track.y,  fmt='%f', delimiter=",")
np.savetxt('Outputdata/pydata.csv',l000015.record_last_track.py, fmt='%f', delimiter=",")
