import numpy as np
import xobjects as xo
import xtrack as xt
import xpart as xp
import xfields as xf
from cpymad.madx import Madx
import matplotlib.pyplot as plt
import pandas as pd
import tfs
from scipy.constants import e as qe
from scipy.constants import m_p
import math
from nlk_tracking import apply_kick

##stored beam parameters for FCCee Z-mode (not all values updated from ttbar settings in example)

bunch_intensity     = 1.7e11  # [1]
energy              = 45.6  # [GeV]
p0c                 = 45.6e9  # [eV]
mass0               = .511e6  # [eV]
phi                 = 15e-3  # [rad] half xing
#u_sr                = 9.2  # [GeV]
#u_bs                = .0114  # [GeV]
#k2_factor           = .4  # [1]
#qx                  = .554  # [1] half arc
#qy                  = .588  # [1]
#qs                  = .0436  # [1]
stor_physemit_x     = 0.27e-09  # [m]
stor_physemit_y     = 1e-12  # [m]
beta_x_ip           = 0.15  # [m]
beta_y_ip           = 0.00078  # [m]
beta_x_sept         = 1981.428866  # [m]
beta_y_sept         = 312.89228  # [m]
alpha_x_sept        = 0.016127  
alpha_y_sept	    = -0.019796
beta_x_kicker       = 193.036  # [m]
beta_y_kicker       = 51.252  # [m]
alpha_x_kicker      = 0.113 
alpha_y_kicker	    = 0.348967
sigma_x_stor_ip     = np.sqrt(stor_physemit_x*beta_x_ip)  # [m]
sigma_px_stor_ip    = np.sqrt(stor_physemit_x/beta_x_ip)  # [m]
sigma_y_stor_ip     = np.sqrt(stor_physemit_y*beta_y_ip)  # [m]
sigma_py_stor_ip    = np.sqrt(stor_physemit_y/beta_y_ip)  # [m]
sigma_x_stor_sept   = np.sqrt(stor_physemit_x*beta_x_sept)  # [m]
sigma_px_stor_sept  = np.sqrt(stor_physemit_x/beta_x_sept)  # [m]
sigma_y_stor_sept   = np.sqrt(stor_physemit_y*beta_y_sept)  # [m]
sigma_py_stor_sept  = np.sqrt(stor_physemit_y/beta_y_sept)  # [m]
sigma_x_stor_kicker    = np.sqrt(stor_physemit_x*beta_x_kicker)  # [m]
sigma_px_stor_kicker   = np.sqrt(stor_physemit_x/beta_x_kicker)  # [m]
sigma_y_stor_kicker    = np.sqrt(stor_physemit_y*beta_y_kicker)  # [m]
sigma_py_stor_kicker   = np.sqrt(stor_physemit_y/beta_y_kicker)  # [m]


df_inj = pd.read_csv("Outputdata/injected_beam_dist.csv",sep='\t')
coords = pd.read_csv("Outputdata/kicked_beam_dist.csv",sep='\t')

#bbeat_df_inj = pd.read_csv("Outputdata/bbeat_injected_beam_dist.csv",sep='\t')
#bbeat_coords = pd.read_csv("Outputdata/bbeat_kicked_beam_dist.csv",sep='\t')
sept15sigCircle=plt.Circle((0,0),15*sigma_x_stor_sept,color='r',fill=False)
kick15sigCircle=plt.Circle((0,0),15*sigma_x_stor_kicker,color='b',fill=False)

fig,ax = plt.subplots(nrows=1,ncols=1,figsize=(9,9))
#ax.add_patch(sept15sigCircle)
#ax.add_patch(kick15sigCircle)
ax.scatter(df_inj["X_sept"],df_inj["PX_sept"],label="septum")
ax.scatter(df_inj["X_mkic"],df_inj["PX_mkic"],label="mkic")
ax.scatter(coords["X"],coords["PX"],label="pre-kicker")
ax.scatter(coords["X"],coords["PX_afterkick"],label="post-kicker")
#ax.scatter(bbeat_df_inj["X"],bbeat_df_inj["PX"],label="bb_septum")
#ax.scatter(bbeat_coords["X"],bbeat_coords["PX"],label="bb_pre-kicker")
#ax.scatter(bbeat_coords["X"],bbeat_coords["PX_afterkick"],label="bb_post-kicker")
ax.legend()

plt.show()


exit()

