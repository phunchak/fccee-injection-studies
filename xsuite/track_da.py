import json
from pathlib import Path
import numpy as np

import xtrack as xt
import xpart as xp
import xobjects as xo
import xdyna as xd


def main():

    filename = Path("inj_lattice_xsuite.json")
    # Load json
    with open(filename, 'r', encoding='utf-8') as fid:
        loaded_dct = json.load(fid)
    line = xt.Line.from_dict(loaded_dct)
    line = line.cycle(name_first_element='frf.1') #start location for track, case sensitive element name
    context = xo.ContextCpu()
    ref_particle = xp.Particles(mass0=xp.ELECTRON_MASS_EV, q0=1, p0c=45.6*10**9, x=0, y=0)
    line.particle_ref = ref_particle
    tracker = xt.Tracker(_context=context, line=line)
    tracker.config.XTRACK_USE_EXACT_DRIFTS = True
    tracker.configure_radiation(model='mean')
    tracker.compensate_radiation_energy_loss()
    twiss = tracker.twiss(eneloss_and_damping=True, method="6d")
    twiss.to_pandas().to_csv("twiss_tracked.csv")
    run_transverse(ref_particle=ref_particle, line=line)
    run_longitudinal(ref_particle=ref_particle, line=line)


def run_transverse(ref_particle, line):

    nturns = 1000
    DA = xd.DA(name='fcc_ee_z_transverse',
               normalised_emittance=[0.27e-9*ref_particle.beta0[0]*ref_particle.gamma0[0],
                                     1e-12*ref_particle.beta0[0]*ref_particle.gamma0[0]],
               max_turns=nturns,
               use_files=False)
    DA.generate_initial_grid(x_min=-40, x_max=40, x_step=2,y_min=0, y_max=60, y_step=4, delta=0.0100) #adjust delta to 1% as a test
    DA.line = line
    DA.track_job()

    da_df = DA.to_pandas(full=True)
    filename='daxy_base_lattice_zmode_xsuite.csv'
    da_df.to_csv(filename)
    print("File Created: {}".format(filename))


def run_longitudinal(ref_particle, line):
    
    nturns = 1000
    DA = xd.DA(name='fcc_ee_z_longitudinal',
    		normalised_emittance=[0.27e-9*ref_particle.beta0[0]*ref_particle.gamma0[0],
                          1e-12*ref_particle.beta0[0]*ref_particle.gamma0[0]],
		max_turns=nturns,
    		use_files=False)

    x_space = np.linspace(0, 40, 11)
    delta_space = np.linspace(-0.015, 0.015, 31)
    x, delta = np.array(np.meshgrid(x_space, delta_space)).reshape(2,-1)

    DA.set_coordinates(x=x, delta=delta)
    DA.line = line
    DA.track_job()

    da_df = DA.to_pandas(full=True)
    filename='daxz_base_lattice_zmode_xsuite.csv'
    da_df.to_csv(filename)
    print("File Created: {}".format(filename))


# Script Mode ------------------------------------------------------------------

if __name__ == "__main__":
    main()

