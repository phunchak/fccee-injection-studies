import numpy as np
import xobjects as xo
import xtrack as xt
import xpart as xp
import xfields as xf
from cpymad.madx import Madx
import matplotlib.pyplot as plt
import pandas as pd
import tfs
from scipy.constants import e as qe
from scipy.constants import m_p
import math
from nlk_tracking import apply_kick

#multiprocessing variables for htcondor
#septum_bbeat_x = %(septBBEATx)s	# factor applied to betax value at the septum (eg. 1.1 for 10% bbeat)
#kicker_bbeat_x = %(kickBBEATx)s # factor applied to betax value at the mki
#delta_mux_err  = %(deltamux)s   # absolute error on phase advance between septum and kicker (in degrees)
septum_bbeat_x = 1	# factor applied to betax value at the septum (eg. 1.1 for 10% bbeat)
kicker_bbeat_x = 1	# factor applied to betax value at the mki
delta_mux_err  = 0	# absolute error on phase advance between septum and kicker (in degrees)
pha_adv_err    = delta_mux_err*math.pi/180	# absolute error on phase advance between septum and kicker (in radians)

##stored beam parameters for FCCee Z-mode (not all values updated from ttbar settings in example)

bunch_intensity     = 1.7e11  # [1]
energy              = 45.6  # [GeV]
p0c                 = 45.6e9  # [eV]
mass0               = .511e6  # [eV]
phi                 = 15e-3  # [rad] half xing
#u_sr                = 9.2  # [GeV]
#u_bs                = .0114  # [GeV]
#k2_factor           = .4  # [1]
#qx                  = .554  # [1] half arc
#qy                  = .588  # [1]
#qs                  = .0436  # [1]
stor_physemit_x     = 0.27e-09  # [m]
stor_physemit_y     = 1e-12  # [m]
beta_x_ip           = 0.15  # [m]
beta_y_ip           = 0.00078  # [m]
beta_x_sept         = 1981.428866*septum_bbeat_x  # [m]
beta_y_sept         = 312.89228  # [m]
alpha_x_sept        = 0.016127  
alpha_y_sept	    = -0.019796
beta_x_mki       = 193.018*kicker_bbeat_x  # [m]
beta_y_mki       = 51.0753  # [m]
alpha_x_mki      = -0.0517
alpha_y_mki	 = -0.3544
beta_x_mkic      = 191.22  # [m]
beta_y_mkic      = 47.536  # [m]
alpha_x_mkic     = -0.047387
alpha_y_mkic	 = -0.002484

sigma_x_stor_ip     = np.sqrt(stor_physemit_x*beta_x_ip)  # [m]
sigma_px_stor_ip    = np.sqrt(stor_physemit_x/beta_x_ip)  # [m]
sigma_y_stor_ip     = np.sqrt(stor_physemit_y*beta_y_ip)  # [m]
sigma_py_stor_ip    = np.sqrt(stor_physemit_y/beta_y_ip)  # [m]
sigma_x_stor_sept   = np.sqrt(stor_physemit_x*beta_x_sept)  # [m]
sigma_px_stor_sept  = np.sqrt(stor_physemit_x/beta_x_sept)  # [m]
sigma_y_stor_sept   = np.sqrt(stor_physemit_y*beta_y_sept)  # [m]
sigma_py_stor_sept  = np.sqrt(stor_physemit_y/beta_y_sept)  # [m]
sigma_x_stor_mki    = np.sqrt(stor_physemit_x*beta_x_mki)  # [m]
sigma_px_stor_mki   = np.sqrt(stor_physemit_x/beta_x_mki)  # [m]
sigma_y_stor_mki    = np.sqrt(stor_physemit_y*beta_y_mki)  # [m]
sigma_py_stor_mki   = np.sqrt(stor_physemit_y/beta_y_mki)  # [m]
sigma_x_stor_mkic    = np.sqrt(stor_physemit_x*beta_x_mkic)  # [m]
sigma_px_stor_mkic   = np.sqrt(stor_physemit_x/beta_x_mkic)  # [m]
sigma_y_stor_mkic    = np.sqrt(stor_physemit_y*beta_y_mkic)  # [m]
sigma_py_stor_mkic   = np.sqrt(stor_physemit_y/beta_y_mkic)  # [m]
#sigma_z             = .00194  # [m] sr
sigma_z             = 0
#sigma_z_tot         = .00254  # [m] sr+bs
#sigma_delta         = .0015  # [m]
sigma_delta         = 0
#sigma_delta_tot     = .00192  # [m]
#beta_s              = sigma_z/sigma_delta  # [m]
#physemit_s          = sigma_z*sigma_delta  # [m]
#physemit_s_tot      = sigma_z_tot*sigma_delta_tot  # [m]
nparticles_stored   = 10000
#n_macroparticles_b1 = int(1e3)
#n_macroparticles_b2 = int(1e6)

#10000 particles in weak beam 1e6 in strong beam

kick_scaling = 1.14

#Function for rotation of a phase advance
def pure_phase_rot(bet1,bet2,alpha1,alpha2,deltamu,pos1,ang1):
	#bet1,alpha1: twiss parameters at start point
	#bet2,alpha2: twiss parameters at end point 
	#deltamu: pure rotation of the phase from start to end point (radians)
	#pos1: positional coordinate
	#ang1: angle coordinate
	M11 = (np.sqrt(bet2/bet1)*(np.cos(deltamu)+alpha1*np.sin(deltamu)))
	M12 = (np.sqrt(bet1*bet2)*np.sin(deltamu))
	M21 = ((((alpha1-alpha2)/np.sqrt(bet1*bet2))*np.cos(deltamu)) - (((1 + alpha1*alpha2)/(np.sqrt(bet1*bet2)))*np.sin(deltamu)))
	M22 = (np.sqrt(bet1/bet2)*(np.cos(deltamu) - alpha2*np.sin(deltamu)))
	M = np.array([[M11,M12],[M21,M22]])
	print(M)
	pos2 = np.zeros_like(pos1)
	ang2 = np.zeros_like(ang1)
	for i,(x,p) in enumerate(zip(pos1,ang1)):
		pos2[i],ang2[i] = M@np.array([[x],[p]])
	
	return pos2,ang2

##Initialize stored beam at MKIC

df_stor = pd.DataFrame(index=range(nparticles_stored), columns=["X_mkic","PX_mkic","Y","PY"])
df_stor["X_mkic"] = sigma_x_stor_mkic  * np.random.randn(nparticles_stored)
df_stor["PX_mkic"]= sigma_px_stor_mkic * np.random.randn(nparticles_stored)
df_stor["Y"] = sigma_y_stor_mkic  * np.random.randn(nparticles_stored)
df_stor["PY"]= sigma_py_stor_mkic * np.random.randn(nparticles_stored)

## Apply MKIC kick
mkiccoords = apply_kick(coordinates = pd.DataFrame(data={"X":df_stor["X_mkic"],"PX":df_stor["PX_mkic"]}),
	kickfile = "/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/kicker_data/KickerFieldProfile.csv",
        kickstrength    = kick_scaling * (0.5/152.105227)*(-0.0079), #0.5/beam_rigidity * kick_factor
	conversion = {'X':1E-3, 'BY':1E-4},
	columns = {'X':0, 'BY':1},
        x_inj = 0.00208, #could update to be based on injected beam actual position
        offset      = 0)

## Phase advance stored beam from mkic to septum
df_stor["X_sept"],df_stor["PX_sept"] = pure_phase_rot(beta_x_mkic,beta_x_sept,alpha_x_mkic,alpha_x_sept,0.5*math.pi,mkiccoords["X"].to_numpy(),mkiccoords["PX_afterkick"].to_numpy())

##injected beam parameters for FCCee Z-mode (not all values updated from ttbar settings in example)
fraction_injected_beam = 0.1
bunch_intensity     = 1.7e11*fraction_injected_beam  # [1]
energy              = 45.6  # [GeV]
p0c                 = 45.6e9  # [eV]
mass0               = .511e6  # [eV]
phi                 = 15e-3  # [rad] half xing
inj_physemit_x      = 0.27e-09  # [m]
inj_physemit_y      = 1e-12  # [m]
sigma_x_inj         = np.sqrt(inj_physemit_x*beta_x_sept)  # [m]
sigma_px_inj        = np.sqrt(inj_physemit_x/beta_x_sept)  # [m]
sigma_y_inj         = np.sqrt(inj_physemit_y*beta_y_sept)  # [m]
sigma_py_inj        = np.sqrt(inj_physemit_y/beta_y_sept)  # [m]
sigma_z             = 0
sigma_delta         = 0
nparticles_injected = int(nparticles_stored*fraction_injected_beam)

##Inialize injected beam at SEPTUM
septum_flag = 1 #flag to either include or ignore the separation from the septum
septumwidth = 3e-3
df_inj 	    = pd.DataFrame(index=range(nparticles_injected), columns=["X_sept","PX_sept","Y","PY"])
stor_beam_sigmas = 5 # size on stored beam in sigmas
inj_beam_sigmas  = 5 # size of injected beam in sigmas
optional_sigma_shift =10+2 # number of (stored beam) sigmas to offset the injected beam beyond simply not overlapping stored and injected

inj_radial_stor_sigx = ((stor_beam_sigmas + septum_flag*(septumwidth / sigma_x_stor_sept) )
 	+ (inj_beam_sigmas * sigma_x_inj/sigma_x_stor_sept) 
 	+ (optional_sigma_shift))

inj_fx = inj_radial_stor_sigx * np.sqrt(stor_physemit_x) * np.cos(phase_at_injection*np.pi/180)

inj_fpx = inj_radial_stor_sigx * np.sqrt(stor_physemit_x) * np.sin(phase_at_injection*np.pi/180)

inj_x = inj_fx * np.sqrt(beta_x_sept)

inj_px= (-1*(alpha_x_sept)*(inj_fx)/np.sqrt(beta_x_sept)) + (inj_fpx/np.sqrt(beta_x_sept))


df_inj["X_sept"] = inj_x  + sigma_x_inj  * np.random.randn(nparticles_injected)
df_inj["PX_sept"]= inj_px + sigma_px_inj * np.random.randn(nparticles_injected)
df_inj["Y"] = sigma_y_inj  * np.random.randn(nparticles_injected)
df_inj["PY"]= sigma_py_inj * np.random.randn(nparticles_injected)


#Transport Beams from Septum to mki and apply kick.

#90 deg phase rotation
inj_df_prekick_x,inj_df_prekick_px = pure_phase_rot(beta_x_sept,beta_x_mki,alpha_x_sept,alpha_x_mki,(0.5*math.pi+pha_adv_err),df_inj["X_sept"].to_numpy(),df_inj["PX_sept"].to_numpy())
stor_df_prekick_x,stor_df_prekick_px = pure_phase_rot(beta_x_sept,beta_x_mki,alpha_x_sept,alpha_x_mki,(0.5*math.pi+pha_adv_err),df_stor["X_sept"].to_numpy(),df_stor["PX_sept"].to_numpy())

#Apply MKI kick
kickfield = "/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/kicker_data/KickerFieldProfile.csv"
kickmag =  kick_scaling*(0.5/152.105227)*(-0.0079), #0.5/beam_rigidity * kick_factor
conversion_dict = {'X':1E-3, 'BY':1E-4}
column_dict = {'X':0, 'BY':1}
x_injection = 0.00208 
mki_offset      = 0

inj_coords = apply_kick(coordinates = pd.DataFrame(data={"X":inj_df_prekick_x,"PX":inj_df_prekick_px}),
	kickfile     = kickfield,
        kickstrength = kickmag,
	conversion   = conversion_dict,
	columns      = column_dict,
        x_inj        = x_injection, 
        offset       = mki_offset)

stor_coords = apply_kick(coordinates = pd.DataFrame(data={"X":stor_df_prekick_x,"PX":stor_df_prekick_px}),
	kickfile     = kickfield,
        kickstrength = kickmag,
	conversion   = conversion_dict,
	columns      = column_dict,
        x_inj        = x_injection, 
        offset       = mki_offset)


df_inj.to_csv("Outputdata/err_inj_beam_sept_dist.csv",sep='\t')
df_stor.to_csv("Outputdata/err_stor_beam_sept_dist.csv",sep='\t')
inj_coords.to_csv("Outputdata/err_mki_kicked_inj_beam_dist.csv",sep='\t')
stor_coords.to_csv("Outputdata/err_mki_kicked_stor_beam_dist.csv",sep='\t')
mkiccoords.to_csv("Outputdata/err_mkic_kicked_stor_beam_dist.csv",sep='\t')

exit()

