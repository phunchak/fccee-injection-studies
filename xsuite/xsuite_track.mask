#This borrows heavily from https://github.com/pkicsiny/xfields/blob/main/examples/002_beambeam/007_beambeam6d_weakstrong_beamstrahlung_tracking.py
#by P.Kicsiny. Adapted for use in top up injection tracking studies.

import numpy as np
import xobjects as xo
import xtrack as xt
import xpart as xp
import xfields as xf
from cpymad.madx import Madx
import matplotlib.pyplot as plt
import pandas as pd
import tfs
from scipy.constants import e as qe
from scipy.constants import m_p
import math

#Set Flags
stored_track = %(STOREDTRACK)s
#stored_track = 1


#LOAD SEQUENCES
mad = Madx()
mad.input("BEAM;")
mad.call("/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/xsuite/ip1tomki.seq")
mad.use("IP1_TO_MKI_L000015")
line1 = xt.Line.from_madx_sequence(mad.sequence['IP1_TO_MKI_L000015'])

mad.call("/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/xsuite/mkitoip2.seq")
mad.use("MKI_TO_IP2_L000015")
line2 = xt.Line.from_madx_sequence(mad.sequence['MKI_TO_IP2_L000015'])

mad.call("/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/xsuite/ip2toip4.seq")
mad.use("IP2_TO_IP4_L000015")
line3 = xt.Line.from_madx_sequence(mad.sequence['IP2_TO_IP4_L000015'])

## Choose a context
context = xo.ContextCpu()         # For CPU
# context = xo.ContextCupy()      # For CUDA GPUs
# context = xo.ContextPyopencl()  # For OpenCL GPUs

## Transfer lattice on context and compile tracking code
ip1_to_mki = xt.Tracker(_context=context, line=line1)
mki_to_ip2 = xt.Tracker(_context=context, line=line2)
ip2_to_ip4 = xt.Tracker(_context=context, line=line3)

## Turn on radiation for each of the tracking segments
ip1_to_mki.configure_radiation(mode='mean')
mki_to_ip2.configure_radiation(mode='mean')
ip2_to_ip4.configure_radiation(mode='mean')

#stored beam parameters for FCCee Z-mode (not all values updated from ttbar settings in example, instead c)
bunch_intensity     = 1.7e11  # [1]
energy              = 45.6  # [GeV]
p0c                 = 45.6e9  # [eV]
mass0               = .511e6  # [eV]
phi                 = 15e-3  # [rad] half xing
physemit_x          = 0.27e-09  # [m]
physemit_y          = 1e-12  # [m]
beta_x_ip              = 0.25  # [m]
beta_y_ip              = 0.00078  # [m]
beta_x_sept            = 1981.428866  # [m]
beta_y_sept            = 312.89228  # [m]
beta_x_mki             = 193.036  # [m]
beta_y_mki             = 51.252  # [m]
sigma_x_stor_ip             = np.sqrt(physemit_x*beta_x_ip)  # [m]
sigma_px_stor_ip            = np.sqrt(physemit_x/beta_x_ip)  # [m]
sigma_y_stor_ip             = np.sqrt(physemit_y*beta_y_ip)  # [m]
sigma_py_stor_ip            = np.sqrt(physemit_y/beta_y_ip)  # [m]
sigma_x_stor_sept           = np.sqrt(physemit_x*beta_x_sept)  # [m]
sigma_px_stor_sept          = np.sqrt(physemit_x/beta_x_sept)  # [m]
sigma_y_stor_sept           = np.sqrt(physemit_y*beta_y_sept)  # [m]
sigma_py_stor_sept          = np.sqrt(physemit_y/beta_y_sept)  # [m]
sigma_x_stor_mki            = np.sqrt(physemit_x*beta_x_mki)  # [m]
sigma_px_stor_mki           = np.sqrt(physemit_x/beta_x_mki)  # [m]
sigma_y_stor_mki            = np.sqrt(physemit_y*beta_y_mki)  # [m]
sigma_py_stor_mki           = np.sqrt(physemit_y/beta_y_mki)  # [m]
sigma_z             = 0
sigma_delta         = 0
nparticles_stored   = int(1e4)
#1e4 particles in weak beam 1e6 in strong beam

#injected beam parameters for FCCee Z-mode (not all values updated from ttbar settings in example)
fraction_injected_beam = 0.1
bunch_intensity     = 1.7e11*fraction_injected_beam  # [1]
energy              = 45.6  # [GeV]
p0c                 = 45.6e9  # [eV]
mass0               = .511e6  # [eV]
phi                 = 15e-3  # [rad] half xing
physemit_x          = 0.27e-09  # [m]
physemit_y          = 1e-12  # [m]
sigma_x_inj         = np.sqrt(physemit_x*beta_x_mki)  # [m]
sigma_px_inj        = np.sqrt(physemit_x/beta_x_mki)  # [m]
sigma_y_inj         = np.sqrt(physemit_y*beta_y_mki)  # [m]
sigma_py_inj        = np.sqrt(physemit_y/beta_y_mki)  # [m]
sigma_z             = 0
sigma_delta         = 0
nparticles_injected = int(nparticles_stored*fraction_injected_beam)

#Dataframe for stored beam particles initial distribution
df_stored_mki = pd.DataFrame(index=range(nparticles_stored), columns=["X","PX","Y","PY"])
df_stored_mki["X"] = sigma_x_stor_mki * np.random.randn(nparticles_stored)
df_stored_mki["PX"]= sigma_px_stor_mki * np.random.randn(nparticles_stored)
df_stored_mki["Y"] = sigma_y_stor_mki * np.random.randn(nparticles_stored)
df_stored_mki["PY"]= sigma_py_stor_mki * np.random.randn(nparticles_stored)

#Dataframe for stored beam particles 
septumwidth=3e-3
df_inj = pd.DataFrame(index=range(nparticles_injected), columns=["X","PX","Y","PY"])
df_inj["X"] = sigma_x_inj  * np.random.randn(nparticles_injected)+5*sigma_x_stor_mki+5*sigma_x_inj+septumwidth*(sigma_x_stor_mki/sigma_x_stor_sept)
df_inj["PX"]= sigma_px_inj * np.random.randn(nparticles_injected)
df_inj["Y"] = sigma_y_inj  * np.random.randn(nparticles_injected)
df_inj["PY"]= sigma_py_inj * np.random.randn(nparticles_injected)

#Collect stored and injected beams as one distribution
#df_stor_and_inj = pd.concat([df_stored_mki,df_inj],axis=0,ignore_index=True)

if stored_track == 1:
	#Create particles object of stored and injected beams
	particles = xp.Particles(
	            _context = context, 
	            q0        = -1,
	            p0c       = p0c,
	            mass0     = mass0,
	            x         = df_stored_mki["X"],
	            y         = df_stored_mki["Y"],
	            zeta      = np.zeros(len(df_stored_mki["X"])),
	            px        = df_stored_mki["PX"],
	            py        = df_stored_mki["PY"],
	            delta     = np.zeros(len(df_stored_mki["X"])),
	            )
else:
	#create particles object of stored and injected beams
	particles = xp.Particles(
	            _context = context, 
	            q0        = -1,
	            p0c       = p0c,
	            mass0     = mass0,
	            x         = df_inj["X"],
	            y         = df_inj["Y"],
	            zeta      = np.zeros(len(df_inj["X"])),
	            px        = df_inj["PX"],
	            py        = df_inj["PY"],
	            delta     = np.zeros(len(df_inj["X"])),
	            )


#create element to apply beam-beam interaction
el_beambeam_b1 = xf.BeamBeamBiGaussian2D(
        _context=context,
        config_for_update = None,
        other_beam_q0=1,
        other_beam_beta0=1,
        other_beam_num_particles=1e7,
        other_beam_Sigma_11=sigma_x_stor_ip,
        other_beam_Sigma_13=None,
        other_beam_Sigma_33=sigma_y_stor_ip,
        min_sigma_diff     = 1e-28,            
)

#Define function to store the turn-by-turn data from the tracking into pandas dataframes
def update_track_dfs(df_x,df_px,df_y,df_py,particles,turn):
    turn_str = "Turn_{}".format(turn)
    df_x.insert(len(df_x.columns),  turn_str,particles.x, False )
    df_px.insert(len(df_px.columns),turn_str,particles.px,False )
    df_y.insert(len(df_y.columns),  turn_str,particles.y, False )
    df_py.insert(len(df_py.columns),turn_str,particles.py,False )
    return df_x, df_px, df_y, df_py

#initialize dataframes for storing tracked data
df_x  = pd.DataFrame(index=range(len(particles.x)), columns=[])
df_px = pd.DataFrame(index=range(len(particles.x)), columns=[])
df_y  = pd.DataFrame(index=range(len(particles.x)), columns=[])
df_py = pd.DataFrame(index=range(len(particles.x)), columns=[])

#Number of turns to be tracked
numturns=2500

#track turn by turn, storing 4-D data every 10th turn
for turn in range(numturns):
    if np.mod(turn,10) == 0:
        df_x, df_px, df_y, df_py = update_track_dfs(df_x,df_px,df_y,df_py,particles,turn)
    mki_to_ip2.track(particles)
    el_beambeam_b1.track(particles)
    ip2_to_ip4.track(particles)
    el_beambeam_b1.track(particles)
    ip1_to_mki.track(particles)
    

df_x.to_csv('Outputdata/df_x.csv',   index=True, sep='\t')
df_px.to_csv('Outputdata/df_px.csv', index=True, sep='\t')
df_y.to_csv('Outputdata/df_y.csv',   index=True, sep='\t')
df_py.to_csv('Outputdata/df_py.csv', index=True, sep='\t')

#fig,ax=plt.subplots(nrows=1,ncols=1,figsize=(9,9))
#ax.scatter(df_x[df_x.columns[-1],df_px[df_px.columns[-1]])
#plt.savefig("Outputdata/final_x_phase")

