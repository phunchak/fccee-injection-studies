import json
from pathlib import Path
from cpymad.madx import Madx
import xtrack as xt
import xpart as xp
import xobjects as xo



def main():

    with Madx() as mad:
        filename = Path("off_p_inj_lattice_xsuite.seq")
        mad.input(
            f"""
                SET, FORMAT="19.15f";
                option,update_from_parent=true; // new option in mad-x as of 2/2019

                CALL, FILE="{filename}";
                BEAM;
                
                USE, SEQUENCE = L000015;

            """
        )
        line = xt.Line.from_madx_sequence(mad.sequence['L000015'],
                                          apply_madx_errors=False,
                                          exact_drift=False, # enabled as part of tracker in the tracking scripts
                                          install_apertures=False)
    # Save to json
    with open(filename.with_suffix('.json'), 'w', encoding='utf-8') as fid:
        json.dump(line.to_dict(), fid, cls=xo.JEncoder)


# Script Mode ------------------------------------------------------------------

if __name__ == "__main__":
    main()

