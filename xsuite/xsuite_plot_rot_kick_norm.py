import numpy as np
import xobjects as xo
import xtrack as xt
import xpart as xp
import xfields as xf
from cpymad.madx import Madx
import matplotlib.pyplot as plt
import pandas as pd
import tfs
from scipy.constants import e as qe
from scipy.constants import m_p
import math
from nlk_tracking import apply_kick
import seaborn as sns

##stored beam parameters for FCCee Z-mode (not all values updated from ttbar settings in example)
bunch_intensity     = 1.7e11  # [1]
energy              = 45.6  # [GeV]
p0c                 = 45.6e9  # [eV]
mass0               = .511e6  # [eV]
phi                 = 15e-3  # [rad] half xing
#u_sr                = 9.2  # [GeV]
#u_bs                = .0114  # [GeV]
#k2_factor           = .4  # [1]
#qx                  = .554  # [1] half arc
#qy                  = .588  # [1]
#qs                  = .0436  # [1]
stor_physemit_x     = 0.27e-09  # [m]
stor_physemit_y     = 1e-12  # [m]
beta_x_ip           = 0.15  # [m]
beta_y_ip           = 0.00078  # [m]
beta_x_sept         = 1981.428866  # [m]
beta_y_sept         = 312.89228  # [m]
alpha_x_sept        = 0.016127  
alpha_y_sept	    = -0.019796
beta_x_mki       = 193.036  # [m]
beta_y_mki       = 51.252  # [m]
alpha_x_mki      = 0.113 
alpha_y_mki	    = 0.348967
beta_x_mkic      = 191.22  # [m]
beta_y_mkic      = 47.536  # [m]
alpha_x_mkic     = -0.047387
alpha_y_mkic     = -0.002484

sigma_x_stor_ip     = np.sqrt(stor_physemit_x*beta_x_ip)  # [m]
sigma_px_stor_ip    = np.sqrt(stor_physemit_x/beta_x_ip)  # [m]
sigma_y_stor_ip     = np.sqrt(stor_physemit_y*beta_y_ip)  # [m]
sigma_py_stor_ip    = np.sqrt(stor_physemit_y/beta_y_ip)  # [m]
sigma_x_stor_sept   = np.sqrt(stor_physemit_x*beta_x_sept)  # [m]
sigma_px_stor_sept  = np.sqrt(stor_physemit_x/beta_x_sept)  # [m]
sigma_y_stor_sept   = np.sqrt(stor_physemit_y*beta_y_sept)  # [m]
sigma_py_stor_sept  = np.sqrt(stor_physemit_y/beta_y_sept)  # [m]
sigma_x_stor_mki    = np.sqrt(stor_physemit_x*beta_x_mki)  # [m]
sigma_px_stor_mki   = np.sqrt(stor_physemit_x/beta_x_mki)  # [m]
sigma_y_stor_mki    = np.sqrt(stor_physemit_y*beta_y_mki)  # [m]
sigma_py_stor_mki   = np.sqrt(stor_physemit_y/beta_y_mki)  # [m]
sigma_x_stor_mkic    = np.sqrt(stor_physemit_x*beta_x_mkic)  # [m]
sigma_px_stor_mkic   = np.sqrt(stor_physemit_x/beta_x_mkic)  # [m]
sigma_y_stor_mkic    = np.sqrt(stor_physemit_y*beta_y_mkic)  # [m]
sigma_py_stor_mkic   = np.sqrt(stor_physemit_y/beta_y_mkic)  # [m]

##injected beam parameters for FCCee Z-mode (not all values updated from ttbar settings in example)
fraction_injected_beam = 0.1
bunch_intensity     = 1.7e11*fraction_injected_beam  # [1]
energy              = 45.6  # [GeV]
p0c                 = 45.6e9  # [eV]
mass0               = .511e6  # [eV]
phi                 = 15e-3  # [rad] half xing
inj_physemit_x      = 0.27e-09/3  # [m]
inj_physemit_y      = 1e-12  # [m]
sigma_x_inj         = np.sqrt(inj_physemit_x*beta_x_sept)  # [m]
sigma_px_inj        = np.sqrt(inj_physemit_x/beta_x_sept)  # [m]
sigma_y_inj         = np.sqrt(inj_physemit_y*beta_y_sept)  # [m]
sigma_py_inj        = np.sqrt(inj_physemit_y/beta_y_sept)  # [m]
sigma_z             = 0
sigma_delta         = 0

# factors for plotting in terms of sigma
sfact_mkic=sigma_x_stor_mkic/np.sqrt(beta_x_mkic)
sfact_sept=sigma_x_stor_sept/np.sqrt(beta_x_sept)
sfact_mki =sigma_x_stor_mki/np.sqrt(beta_x_mki)

##Load beam distribution data
df_inj = pd.read_csv("Outputdata/inj_beam_sept_dist.csv",sep='\t')
df_stor = pd.read_csv("Outputdata/stor_beam_sept_dist.csv",sep='\t')
inj_coords = pd.read_csv("Outputdata/mki_kicked_inj_beam_dist.csv",sep='\t')
stor_coords = pd.read_csv("Outputdata/mki_kicked_stor_beam_dist.csv",sep='\t')
mkiccoords = pd.read_csv("Outputdata/mkic_kicked_stor_beam_dist.csv",sep='\t')

##Load MKI Field Data
profile_df = pd.read_csv("../kicker_data/KickerFieldProfile.csv",sep='\t',names=['x','By_noDip','By_Dip'],skipfooter=1, engine='python')
profile_df["x_meters"]=profile_df["x"]/1000
profile_df["x_mkic_norm"]=profile_df["x_meters"]/np.sqrt(beta_x_mkic)
profile_df["x_mki_norm"]=profile_df["x_meters"]/np.sqrt(beta_x_mki)
profile_df["By_Dip_Tesla"]=profile_df["By_Dip"]*1e-4
x_injection = 0.00208
index = abs(profile_df['x_meters'] - x_injection).idxmin()
profile_df["By_Dip_Tesla_normalised"]= profile_df["By_Dip_Tesla"]/profile_df["By_Dip_Tesla"].loc[index]

##translate coordinates to normalized phase space
mkicfx=mkiccoords["X"]/np.sqrt(beta_x_mkic)
premkicfpx=mkiccoords["PX"]*np.sqrt(beta_x_mkic)+alpha_x_mkic*mkicfx
postmkicfpx=mkiccoords["PX_afterkick"]*np.sqrt(beta_x_mkic)+alpha_x_mkic*mkicfx

inj_septfx=df_inj["X_sept"]/np.sqrt(beta_x_sept)
inj_septfpx=df_inj["PX_sept"]*np.sqrt(beta_x_sept)+alpha_x_sept*inj_septfx
stor_septfx=df_stor["X_sept"]/np.sqrt(beta_x_sept)
stor_septfpx=df_stor["PX_sept"]*np.sqrt(beta_x_sept)+alpha_x_sept*stor_septfx

inj_mkifx=inj_coords["X"]/np.sqrt(beta_x_mki)
inj_premkifpx=inj_coords["PX"]*np.sqrt(beta_x_mki)+alpha_x_mki*inj_mkifx
inj_postmkifpx=inj_coords["PX_afterkick"]*np.sqrt(beta_x_mki)+alpha_x_mki*inj_mkifx

stor_mkifx=stor_coords["X"]/np.sqrt(beta_x_mki)
stor_premkifpx=stor_coords["PX"]*np.sqrt(beta_x_mki)+alpha_x_mki*stor_mkifx
stor_postmkifpx=stor_coords["PX_afterkick"]*np.sqrt(beta_x_mki)+alpha_x_mki*stor_mkifx

##Define 15 and 17 sigma circles and septum rectangle for plotting
circ15sigA=plt.Circle((0,0),15,color='c',fill=False, label="15\u03c3 (Primary Collimator)")
circ15sigB=plt.Circle((0,0),15,color='c',fill=False, label="15\u03c3 (Primary Collimator)")
circ15sigC=plt.Circle((0,0),15,color='c',fill=False, label="15\u03c3 (Primary Collimator)")
circ17sigA=plt.Circle((0,0),17,color='m',fill=False, label="17\u03c3 (Septum)")
circ17sigB=plt.Circle((0,0),17,color='m',fill=False, label="17\u03c3 (Septum)")
septRect=plt.Rectangle((17,-40),width=3e-3/sigma_x_stor_sept,height=80,color="black",hatch="/", label="Septum")

##calculate How many particles are lost on the septum
npartStor=len(stor_septfx)
npartInj=len(inj_septfx)
nlostSept=sum(stor_septfx >= (17*sigma_x_stor_sept/np.sqrt(beta_x_sept)))
nlostInj=sum(np.sqrt(inj_mkifx**2 + inj_postmkifpx**2) >= (15*sigma_x_stor_mki/np.sqrt(beta_x_mki)))

#calculate injected beam centroid in normalized phase space after injection (inj_mkifx,inj_postmkifpx)
inj_centfx  = sum(inj_mkifx)/npartInj
inj_centfpx = sum(inj_postmkifpx)/npartInj

inj_normR = np.sqrt(inj_centfx**2+inj_centfpx**2)
inj_normR_sig = inj_normR/(sigma_x_stor_mki/np.sqrt(beta_x_mki))

print("\n-------------------------------------------------------------------------------")
print("\n\n")
print("Injected beam radius from (0,0) in normalized phase space = {:.2f}\u03c3".format(inj_normR_sig))
print("\n")
print("Total Particles in stored beam = {}".format(npartStor))
print("Stored Beam Particles lost on Septum = {}".format(nlostSept))
print("Percentage of Stored Beam Lost = {:.2f}%".format(nlostSept/npartStor*100))
print("\n")
print("Total Particles in injected beam = {}".format(npartInj))
print("Injected Beam Particles not stored in 15\u03c3 = {}".format(nlostInj))
print("Percentage of Stored Beam Lost = {:.2f}%".format(nlostInj/npartInj*100))
print("\n\n")
print("-------------------------------------------------------------------------------\n")


#Plot beams, septum, 15sigma and 17sigma rings in normalized phase space, and MKI/MKIC Field overlaid.
fig,ax = plt.subplots(nrows=1,ncols=3,figsize=(20,6),)
ax02 = ax[0].twinx() 
ax22 = ax[2].twinx() 

#set order of twinned axis so that mki field is plotted behind particle distributions.
ax[0].set_zorder(1)
ax[0].patch.set_visible(False)
ax[2].set_zorder(1)
ax[2].patch.set_visible(False)

dotsize=0.1

mkicFact=np.sqrt(beta_x_mkic)/sigma_x_stor_mkic
septFact=np.sqrt(beta_x_sept)/sigma_x_stor_sept
mkiFact=np.sqrt(beta_x_mki)/sigma_x_stor_mki


#plotting at MKIC
ax02.plot(profile_df["x_mkic_norm"]*mkicFact,profile_df["By_Dip_Tesla_normalised"],linewidth=2,color="r",label="MKIC Field")
ax[0].add_patch(circ15sigA)
ax[0].add_patch(circ17sigA)
ax[0].scatter(mkicfx*mkicFact, premkicfpx*mkicFact, label="Beam Pre-MKIC",  s=dotsize)
ax[0].scatter(mkicfx*mkicFact, postmkicfpx*mkicFact,label="Beam Post-MKIC", s=dotsize)
#plotting at Septum
ax[1].add_patch(circ15sigB)
ax[1].add_patch(circ17sigB)
ax[1].add_patch(septRect)
ax[1].scatter(inj_septfx*septFact, inj_septfpx*septFact,    label="Inj. Beam at Septum",s=dotsize)
ax[1].scatter(stor_septfx*septFact, stor_septfpx*septFact,    label="Stor. Beam at Septum",s=dotsize)
#plotting at MKI
ax22.plot(profile_df["x_mki_norm"]*mkiFact,profile_df["By_Dip_Tesla_normalised"],linewidth=2,color="r",label="MKI Field")
ax[2].add_patch(circ15sigC)
ax[2].scatter(inj_mkifx*mkiFact,  inj_premkifpx*mkiFact,   label="Inj. Beam Pre-MKI",  s=dotsize)
ax[2].scatter(inj_mkifx*mkiFact,  inj_postmkifpx*mkiFact,  label="Inj. Beam Post-MKI", s=dotsize)
ax[2].scatter(stor_mkifx*mkiFact, stor_premkifpx*mkiFact,   label="Stor. Beam Pre-MKI",  s=dotsize)
ax[2].scatter(stor_mkifx*mkiFact, stor_postmkifpx*mkiFact,  label="Stor. Beam Post-MKI", s=dotsize)

textsize=12
#debug vertical lines
#ax[1].axvline(x=5)
#ax[1].axvline(x=15)
#ax[1].axvline(x=(15+2))
#ax[1].axvline(x=((15+2)+3e-3/sigma_x_stor_sept))
ax[1].axvline(x=((15+2)+3e-3/sigma_x_stor_sept+5*sigma_x_inj/sigma_x_stor_sept))


ax[0].set_xlabel("$\hat{X}$", fontsize=textsize)
ax[0].set_ylabel("$\hat{PX}$", fontsize=textsize)
ax[1].set_xlabel("$\hat{X}$", fontsize=textsize)
ax[2].set_xlabel("$\hat{X}$", fontsize=textsize)
#ax.scatter(bbseptfx,bbseptfpx,label="bbseptum")
#ax.scatter(bbkickfx,bbprekickfpx,label="bbprekick")
#ax.scatter(bbkickfx,bbpostkickfpx,label="bbpostkick")
lgnd0 = ax[0].legend(fontsize=textsize)
for handle in lgnd0.legendHandles[3:]:
	handle.set_sizes([15])
lgnd1 = ax[1].legend(fontsize=textsize)
for handle in lgnd1.legendHandles[3:]:
	handle.set_sizes([15])
lgnd2 = ax[2].legend(fontsize=textsize)
for handle in lgnd2.legendHandles[2:]:
	handle.set_sizes([15])
ax[0].set_xlim([-40,40])
ax[0].set_ylim([-40,40])
ax[1].set_xlim([-40,40])
ax[1].set_ylim([-40,40])
ax[2].set_xlim([-40,40])
ax[2].set_ylim([-40,40])
ax[0].tick_params(axis='both', which='major', labelsize=textsize)
ax[1].tick_params(axis='both', which='major', labelsize=textsize)
ax[2].tick_params(axis='both', which='major', labelsize=textsize)
ax02.yaxis.set_ticks([])
ax22.yaxis.set_ticks([])

fig.tight_layout()
plt.show()


exit()

