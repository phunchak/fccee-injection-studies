import json
import numpy as np
import xobjects as xo
import xtrack as xt
import xpart as xp
import xfields as xf
from cpymad.madx import Madx
import tfs
import pandas as pd

bunch_intensity     = 1.7e11  # [1]
energy              = 45.6  # [GeV]
p0c                 = 45.6e9  # [eV]
mass0               = .511e6  # [eV]
stor_physemit_x          = 0.27e-09  # [m]
stor_physemit_y          = 1e-12  # [m]
beta_x_kicker          = 193.036  # [m]
beta_y_kicker          = 51.252  # [m]
sigma_x_stor_kicker    = np.sqrt(stor_physemit_x*beta_x_kicker)  # [m]
sigma_px_stor_kicker   = np.sqrt(stor_physemit_x/beta_x_kicker)  # [m]
sigma_y_stor_kicker    = np.sqrt(stor_physemit_y*beta_y_kicker)  # [m]
sigma_py_stor_kicker   = np.sqrt(stor_physemit_y/beta_y_kicker)  # [m]
nparticles_stored      = 1
## Choose a context
context = xo.ContextCpu()         # For CPU

##Initialize stored beam at MKI

df_stored_kicker = pd.DataFrame(index=range(nparticles_stored), columns=["X","PX","Y","PY"])
df_stored_kicker["X"] = sigma_x_stor_kicker * np.random.randn(nparticles_stored)
df_stored_kicker["PX"]= sigma_px_stor_kicker * np.random.randn(nparticles_stored)
df_stored_kicker["Y"] = sigma_y_stor_kicker * np.random.randn(nparticles_stored)
df_stored_kicker["PY"]= sigma_py_stor_kicker * np.random.randn(nparticles_stored)

##Create particles object
particles = xp.Particles(
            _context = context,
            q0        = -1,
            p0c       = p0c,
            mass0     = mass0,
            x         = 0,
            y         = 0,
            zeta      = 0,
            px        = 0,
            py        = 0,
            delta     = 0,
            )

# Load from json
with open('l000015_z_lattice.json', 'r') as fid:
    loaded_dct = json.load(fid)
line_2 = xt.Line.from_dict(loaded_dct)

line_2.particle_ref = particles

#################
# Build tracker #
#################

tracker = xt.Tracker(line=line_2)

#########
# Twiss #
#########

tw = tracker.twiss()

import matplotlib.pyplot as plt

plt.close('all')

fig1 = plt.figure(1, figsize=(6.4, 4.8*1.5))
spbet = plt.subplot(3,1,1)
spco = plt.subplot(3,1,2, sharex=spbet)
spdisp = plt.subplot(3,1,3, sharex=spbet)

spbet.plot(tw['s'], tw['betx'])
spbet.plot(tw['s'], tw['bety'])

spco.plot(tw['s'], tw['x'])
spco.plot(tw['s'], tw['y'])

spdisp.plot(tw['s'], tw['dx'])
spdisp.plot(tw['s'], tw['dy'])

spbet.set_ylabel(r'$\beta_{x,y}$ [m]')
spco.set_ylabel(r'(Closed orbit)$_{x,y}$ [m]')
spdisp.set_ylabel(r'$D_{x,y}$ [m]')
spdisp.set_xlabel('s [m]')

fig1.suptitle(
    r'$q_x$ = ' f'{tw["qx"]:.5f}' r' $q_y$ = ' f'{tw["qy"]:.5f}' '\n'
    r"$Q'_x$ = " f'{tw["dqx"]:.2f}' r" $Q'_y$ = " f'{tw["dqy"]:.2f}'
    r' $\gamma_{tr}$ = '  f'{1/np.sqrt(tw["momentum_compaction_factor"]):.2f}'
)

fig1.subplots_adjust(left=.15, right=.92, hspace=.27)
plt.show()
