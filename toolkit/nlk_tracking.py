"""
Nonlinear Kicker Tracking
-------------
The ``nlk_tracking`` scripts allows to perform tracking studies on arbitrary magnetic field profiles in MAD-X.
The tracking output in form of a MAD-X trackone file is loaded at a given element and for a given turn.
The field profile stored in a csv is then loaded. Currently, only the vertical field strength for given horizontal coordinate is used.
A linear interpolation for the field profile is used and the strength relativ to the field at a given  position times the specified kickstrength is applied to the px coordinate.
The coordinates are then saved into a file which can be sued for MAD-X tracking, and optionally a csv containing the coordinates before and after the transformation.

*--Required--*
- **trackone_file** *(str)*: Path to the trackone file to which kicks are applied.
- **kickprofile_file** *(str)*: Path to the file where the B_y field for different x coordinates is given.
- **kickstrength** *(float)*: Strength of the nonlinear kicker
- **madx_file** *(str)*: Path to the file where the coordinates after the kick are stored.
- **element** *(str)*: Element at which coordinates should be used for the transform.
- **injection_point** *(float)*: X-ccordinate which is used to normalize the B_y field.

*--Optional--*
- **turn** *(int)*: Turn at which coordinates should be taken, using numpy index convention. By default -1 is used, that is, taking the last turn.
- **df_file** *(str)*: Path to the file where the coordinates dataframe is saved to. If not given, file will not be saved.
- **kickprofile_columns** *(DictAsString)*: Dict specifiying which columns should be used for the field profile. Default is {'X':0, 'BY':1}
- **conversion** *(DictAsString)*: Conversion factors applied on the data. Default is X mm->m and By G -> T i.e. {'X':1E-3, 'BY':1E-4}

"""

from collections import namedtuple
from datetime import datetime
from pathlib import Path
from typing import List, Union, Dict

import numpy as np
from scipy import interpolate
import pandas as pd

from generic_parser import EntryPointParameters, entrypoint
from generic_parser.entry_datatypes import DictAsString


# Constants --------------------------------------------------------------------

PLANES=('X', 'Y', 'PX', 'PY')
HEADER = "@"
NAMES = "*"
TYPES = "$"
SEGMENTS = "#segment"
SEGMENT_MARKER = ("start", "end")
COLX = "X"
COLY = "Y"
COLPX = "PX"
COLPY = "PY"
COLTURN = "TURN"
COLPARTICLE = "NUMBER"
DATE = "DATE"
TIME = "TIME"
TIME_FORMAT = "%d/%m/%y %H.%M.%S"
Segment = namedtuple("Segment", ["number", "turns", "particles", "element", "name"])


# Script arguments -------------------------------------------------------------
def get_params():
    params = EntryPointParameters()
    params.add_parameter(
        name="trackone_file",
        type=str,
        required=True,
        help="Path to the trackone file to which kicks are applied.",
    )
    params.add_parameter(
        name="kickprofile_file",
        type=str,
        required=True,
        help="Path to the file where the B_y field for different x coordinates is given.",
    )
    params.add_parameter(
        name="kickprofile_columns",
        type=DictAsString,
        default={'X':0, 'BY':1},
        help="Dict specifiying which columns should be used for the field profile. Default is {'X':0, 'BY':1}",
    )
    params.add_parameter(
        name="conversion",
        type=DictAsString,
        default={'X':1E-3, 'BY':1E-4},
        help="Conversion factors applied on the data. Default is X mm->m and By G -> T i.e. {'X':1E-3, 'BY':1E-4}",
    )
    params.add_parameter(
        name="kickstrength",
        type=float,
        required=True,
        help="Strength of the nonlinear kicker",
    )
    params.add_parameter(
        name="injection_point",
        type=float,
        required=True,
        help="X-ccordinate which is used to normalize the B_y field.",
    )
    params.add_parameter(
        name="madx_file",
        type=str,
        required=True,
        help="Path to the file where the coordinates after the kick are stored.",
    )
    params.add_parameter(
        name="df_file",
        type=str,
        help="Path to the file where the coordinates dataframe is saved to. If not given, file will not be saved.",
    )
    params.add_parameter(
        name="element",
        type=str,
        required=True,
        help="Element at which coordinates should be used for the transform.",
    )
    params.add_parameter(
        name="turn",
        type=int,
        default=-1,
        help="Turn at which coordinates should be taken, using numpy index convention. By default -1 is used, that is, taking the last turn.",
    )
    params.add_parameter(
        name="nlk_offset",
	type=float,
	default=0.0,
	help="The horizontal misalignment of the NLK magnet, resulting in translation of the magnetic field profile",
    )
    	

    return params

# Entrypoint -------------------------------------------------------------------
@entrypoint(get_params(), strict=True)
def main(opt):
    opt = _check_opts(opt)
    trackone = read_tbt(opt.trackone_file)
    coordinates = get_coordinates_from_trackone(trackone, opt.element, opt.turn)
    coordinates = apply_kick(coordinates, opt.kickprofile_file, opt.kickstrength, opt.conversion, opt.kickprofile_columns, opt.injection_point, opt.nlk_offset)

    write_madx_track_start_coordinates(coordinates, opt.madx_file, columns={'X':'X','PX':'PX_afterkick','Y':'Y','PY':'PY'})
    if opt.df_file != None:
        coordinates.to_csv(opt.df_file, sep='\t')


def _check_opts(opt):

    for filename in ["trackone_file", "kickprofile_file"]:
        if Path(opt[filename]).is_file():
            opt[filename] = Path(opt[filename])
        else:
            raise OSError(f"Path for {filename} appears to be invalid.")

    if not all(key in opt.kickprofile_columns.keys() for key in ('X', 'BY')):
        raise ValueError('Kickprofile columns dict needs X and BY as keys.')
    if not all(key in opt.conversion.keys() for key in ('X', 'BY')):
        raise ValueError('Conversion columns dict needs X and BY as keys.')

    return opt


def get_coordinates_from_trackone(trackone:List, element:str, turn:int):
    coordinates = pd.DataFrame()

    for particle in range(len(trackone)):
        coordlist = {plane: trackone[particle][plane].loc[element].to_numpy()[turn] for plane in PLANES}
        coordinates = coordinates.append(pd.DataFrame(data=coordlist, index=[particle]), ignore_index=True)

    return coordinates


def apply_kick(coordinates:pd.DataFrame, kickfile:Path, kickstrength:float, conversion:Dict, columns:Dict, x_inj:float, offset:float):
    """
    Loads the field strength file kickfile, converts units using the conversion dict, then interpolates the relative field strength over X coordinate.
    Uses the px coordinate from the coordinates dataframe and applies kick using the interpolation function and the kick strength.
    Returns pandas dataframe with old and kicked coordinates.
    
    Added offset (in meters) input to allow for misalignment of the NLK
    """

    
    bfield_df = pd.read_csv(kickfile, names=columns.keys(), usecols=columns.values(), delimiter='\t').apply(pd.to_numeric, errors='coerce').dropna()

    # convert from G to T and mm to m
    bfield_df['BY'] = bfield_df['BY']*conversion['BY']
    bfield_df['X'] = bfield_df['X']*conversion['X']+offset

    bfield_fct = interpolate.interp1d(bfield_df['X'], bfield_df['BY'], fill_value='extrapolate')
    #we subtract the field at the zero coordinate from the magnetic field to shift the whole distribution down and have zero on-axis field
    coordinates['PX_afterkick'] = coordinates['PX'] - kickstrength * (bfield_fct(coordinates['X'])-bfield_fct(0))/bfield_fct(x_inj+offset)
    return coordinates


def write_madx_track_start_coordinates(df:pd.DataFrame, filename:str, columns={'X':'X','PX':'PX','Y':'Y','PY':'PY'}):

    with open(filename, 'w') as f:
        for _, line in df.iterrows():
            f.write(f'START, X={line[columns["X"]]}, '\
                           f'PX={line[columns["PX"]]}, '\
                           f'Y={line[columns["Y"]]}, '\
                           f'PY={line[columns["PY"]]};\r\n')

# TbT reader adapted from https://raw.githubusercontent.com/pylhc/omc3/master/omc3/tbt/reader_ptc.py

def read_tbt(file_path: Union[str, Path]):
    """
    Reads TbtData object from ``PTC`` **trackone** output.

    Args:
        file_path (Union[str, Path]): path to a file containing TbtData.

    Returns:
        A ``TbtData`` object with the loaded data.
    """
    file_path = Path(file_path)
    
    lines: List[str] = file_path.read_text().splitlines()

    # header
    date, header_length = _read_header(lines)
    lines = lines[header_length:]

    # parameters
    bpms, particles, column_indices, n_turns, n_particles = _read_from_first_turn(lines)

    # data (read into dict first for speed, then convert to DF)
    matrices = [
        {p: {bpm: np.zeros(n_turns) for bpm in bpms} for p in PLANES} for _ in range(n_particles)
    ]
    matrices = _read_data(lines, matrices, column_indices)
    for bunch in range(n_particles):
        for plane in PLANES:
            matrices[bunch][plane] = pd.DataFrame(matrices[bunch][plane]).transpose()

    return matrices
    

# Read all lines ---------------------------------------------------------------


def _read_header(lines: List[str]) -> datetime:
    """Reads header length and datetime from header."""
    idx_line = 0
    date_str = {k: None for k in [DATE, TIME]}
    for idx_line, line in enumerate(lines):
        parts = line.strip().split()
        if len(parts) == 0:
            continue

        if parts[0] != HEADER:
            break

        if parts[1] in date_str.keys():
            date_str[parts[1]] = parts[-1].strip("'\" ")

    if any(ds is None for ds in date_str.keys()):
        
        return datetime.utcnow(), idx_line

    return datetime.strptime(f"{date_str[DATE]} {date_str[TIME]}", TIME_FORMAT), idx_line


def _read_from_first_turn(lines: List[str]) -> tuple:
    """
    Reads the BPMs, particles, column indices and number of turns and particles from the data of
    the first turn.
    """
    bpms = []
    particles = []
    column_indices = None
    n_turns = 0
    n_particles = 0
    first_segment = True

    for line in lines:
        parts = line.strip().split()
        if len(parts) == 0 or parts[0] in [HEADER, TYPES]:
            continue

        if parts[0] == NAMES:  # read column names
            if column_indices is not None:
                raise KeyError(f"{NAMES} are defined twice in tbt file!")
            column_indices = _parse_column_names_to_indices(parts[1:])
            continue

        if parts[0] == SEGMENTS:  # read segments, append to index
            segment = Segment(*parts[1:])
            if segment.name == SEGMENT_MARKER[0]:  # start of first segment
                n_turns = int(segment.turns) - 1
                n_particles = int(segment.particles)

            elif segment.name == SEGMENT_MARKER[1]:  # end of first segment
                break

            else:
                first_segment = False
                bpms.append(segment.name)

        elif first_segment:
            if column_indices is None:
                raise IOError("Columns not defined in Tbt file!")

            new_data = _parse_data(column_indices, parts)
            particle = int(float(new_data[COLPARTICLE]))
            particles.append(particle)

    if len(particles) == 0:
        raise IOError("No data found in TbT file!")
    return bpms, particles, column_indices, n_turns, n_particles


def _read_data(lines: List[str], matrices, column_indices):
    """Read the data into the matrices."""
    segment = None
    column_map = {"X": COLX, "Y": COLY, "PX": COLPX, "PY": COLPY}

    for line in lines:
        parts = line.strip().split()
        if len(parts) == 0 or parts[0] in (HEADER, TYPES, NAMES):
            continue

        if parts[0] == SEGMENTS:  # start of a new segment
            segment = Segment(*parts[1:])
            continue

        if segment is None:
            raise IOError("Data defined before Segment definition!")

        if segment.name in SEGMENT_MARKER:
            continue

        data = _parse_data(column_indices, parts)
        part_id = int(float(data[COLPARTICLE])) - 1
        turn_nr = int(float(data[COLTURN])) - 1
        for plane in PLANES:
            matrices[part_id][plane][segment.name][turn_nr] = float(data[column_map[plane]])
    return matrices


# Parse single lines -----------------------------------------------------------


def _parse_data(column_indices, parts) -> dict:
    """Converts the ``parts`` into a dictionary based on the indices in ``column_indices``."""
    return {col: parts[col_idx] for col, col_idx in column_indices.items()}


def _parse_column_names_to_indices(parts) -> dict:
    """Parses the column names from the line into a dictionary with indices."""
    col_idx = {k: None for k in [COLX, COLY, COLPX, COLPY, COLTURN, COLPARTICLE]}
    for idx, column_name in enumerate(parts):
        if column_name not in col_idx:
            continue
        if col_idx[column_name] is not None:
            raise KeyError(f"'{column_name}' is defined twice.")
        col_idx[column_name] = idx
    missing = [c for c in col_idx.values() if c is None]
    if any(missing):
        raise ValueError(f"The following columns are missing in ptc file: '{str(missing)}'")
    return col_idx


# Script Mode ------------------------------------------------------------------

if __name__ == "__main__":
    main()
