import numpy as np 
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse, Rectangle

import itertools

def plot_DA_as_line(ax, df, mapping={'X':'SIGMAX', 'Y':'SIGMAY', 'Loss': 'Lost'}):
    
    da_boundary= find_DA(df, mapping)
    ax.plot(da_boundary['X'],
            da_boundary['DA'],
            linestyle='-',
            linewidth=2,
            color='black',
            label='DA'
           )
    
def find_DA(df, mapping):
    
    df=df.groupby(mapping['X'])
    
    xaxis=[]
    boundary=[]
    for x, data in df:
        xaxis.append(x)
        # finds the largest amplitude under which all particles have survived
        # cum sum over the bool column Loss, all entries with 0 have stable particle below, find index of last entry and use amplitude
        data=data[data[mapping['Y']]>0]
        surviving_particles = data.loc[data[mapping['Loss']].cumsum()==0]
        if surviving_particles.empty:
            boundary.append(0)
        else:
            boundary.append(
                data.loc[surviving_particles.index[-1]][mapping['Y']]
                )
    return pd.DataFrame(data={'X': xaxis, 'DA':boundary}) 
        
        
def add_beam_ellipse(ax, loc=[0,0], beam={'betax':1, 'emitx':1e-9, 'nx':5, 'betay':1, 'emity':1e-9, 'ny':5}, sigma=False):
    
    if sigma:
        beamsize = {plane: beam[f'n{plane.lower()}'] for plane in ('X', 'Y')}
    else:
        beamsize = {plane: beam[f'n{plane.lower()}']*np.sqrt( beam[f'beta{plane.lower()}']*beam[f'emit{plane.lower()}'] ) for plane in ('X', 'Y')}
    
    ell = Ellipse(xy=loc, width=2*beamsize['X'], height=2*beamsize['Y'], edgecolor='black', fill=None, linewidth=2)
    ax.add_artist(ell)


def add_septum(ax, center_loc=[0,0], height=1, width=1):
    
    sept = Rectangle(xy=center_loc-np.array([width,height])/2., width=width, height=height, edgecolor='black', fill=None, linewidth=2)
    ax.add_artist(sept) 


def grid_plot_surviving_particles(ax, df, mapping={'X':'SIGMAX', 'Y':'SIGMAY', 'Loss': 'Lost'}):

    ax.scatter(df[mapping['X']][df[mapping['Loss']]==False], df[mapping['Y']][df[mapping['Loss']]==False], color='blue', alpha=0.5, label='Surviving')
    ax.scatter(df[mapping['X']][df[mapping['Loss']]==True], df[mapping['Y']][df[mapping['Loss']]==True], color='red', alpha=0.5, label='Lost')


def contour_plot_surviving_turns(ax, df,  mapping={'X':'SIGMAX', 'Y':'SIGMAY', 'Turns': 'Turns'}):

    contour_df = df.pivot(mapping['X'], mapping['Y'], mapping['Turns'])
    x,y=np.meshgrid(contour_df.columns.to_numpy(), contour_df.index.to_numpy())
    cs = ax.contourf(x,
                        y,
                        contour_df.to_numpy(),
                        cmap='inferno')
    
    cbar=plt.colorbar(cs)
    cbar.ax.set_ylabel("No. of surviving turns", fontsize=16)
    cbar.ax.tick_params(axis='both', which='major', labelsize=12)


if __name__ == "__main__":

    sigma_limits=[-12,12]
    coord_limits=[0,0.001]

    sigma_range=np.array(list(itertools.product(np.linspace(sigma_limits[0], sigma_limits[1], 25, endpoint=True), repeat=2)))
    coord_range=np.array(list(itertools.product(np.linspace(coord_limits[0], coord_limits[1], 25, endpoint=True), repeat=2)))


    df = pd.DataFrame(data={ 'X': coord_range[:, 0],
                             'Y': coord_range[:, 1],
                             'SIGMAX': sigma_range[:, 0],
                             'SIGMAY': sigma_range[:, 1],
                             'Turns_survived': np.zeros(len(coord_range[:,0])) })

    df['Lost_SIG'] = np.where( df['SIGMAX']**2+df['SIGMAY']**2 < 10**2, 0, 1 )    
    df['Lost_Coord'] = np.where( df['X']**2+df['Y']**2 < 0.0006**2, 0, 1 )

    df.loc[ (df['X']**2+df['Y']**2 < 0.0008**2) ,'Turns_survived']=10
    df.loc[ (df['X']**2+df['Y']**2 < 0.0007**2) ,'Turns_survived']=30
    df.loc[ (df['X']**2+df['Y']**2 < 0.0006**2) ,'Turns_survived']=50

    fig, ax = plt.subplots(nrows=3, ncols=1, figsize=(9,9))

    grid_plot_surviving_particles(ax[0], df, mapping={'X':'SIGMAX', 'Y':'SIGMAY', 'Loss':'Lost_SIG'})
    grid_plot_surviving_particles(ax[1], df, mapping={'X':'X', 'Y':'Y', 'Loss':'Lost_Coord'})

    contour_plot_surviving_turns(ax[2], df,  mapping={'X':'X', 'Y':'Y', 'Turns':'Turns_survived'})

    plot_DA_as_line(ax[0], df, mapping={'X':'SIGMAX', 'Y':'SIGMAY', 'Loss':'Lost_SIG'})
    plot_DA_as_line(ax[1], df, mapping={'X':'X', 'Y':'Y', 'Loss':'Lost_Coord'})
    plot_DA_as_line(ax[2], df, mapping={'X':'X', 'Y':'Y', 'Loss':'Lost_Coord'})

    add_beam_ellipse(ax[0], beam={'betax':1, 'emitx':1, 'nx':5, 'betay':1, 'emity':1, 'ny':5}, sigma=True)
    add_beam_ellipse(ax[0], loc=[7,0], beam={'betax':1, 'emitx':1, 'nx':1, 'betay':1, 'emity':1, 'ny':1}, sigma=True)
    add_septum(ax[0], center_loc=[5.5,0], width=1, height=12)

    add_beam_ellipse(ax[1], beam={'betax':600, 'emitx':1e-11, 'nx':5, 'betay':300, 'emity':1e-11, 'ny':5}, sigma=False)
    add_beam_ellipse(ax[1], loc=[0.00055,0], beam={'betax':600, 'emitx':1e-11, 'nx':1, 'betay':300, 'emity':1e-11, 'ny':1}, sigma=False)
    add_septum(ax[1], center_loc=[0.00045,0], width=0.00005, height=0.001)

    add_beam_ellipse(ax[2], beam={'betax':600, 'emitx':1e-11, 'nx':5, 'betay':300, 'emity':1e-11, 'ny':5}, sigma=False)
    add_beam_ellipse(ax[2], loc=[0.00055,0], beam={'betax':600, 'emitx':1e-11, 'nx':1, 'betay':300, 'emity':1e-11, 'ny':1}, sigma=False)
    add_septum(ax[2], center_loc=[0.00045,0], width=0.00005, height=0.001)

    ax[0].set_xlim(sigma_limits)
    ax[0].set_ylim(sigma_limits)

    ax[1].set_xlim(coord_limits)
    ax[1].set_ylim(coord_limits)
    
    plt.show()
