import tfs
import matplotlib.pyplot as plt
import numpy as np
import pathlib as Path
from datetime import datetime
import pandas as pd

#read in error effect dataframes
dataFolder 	= "lattice_errors"
Jobs		= tfs.read('./{}/Jobs.tfs'.format(dataFolder), index='JobId')
numjobs		= max(Jobs.index)+1
error_effects	= tfs.read('error_effects_comparison.tfs', index='job_number')

full_df = pd.concat([Jobs.loc[:,~Jobs.columns.isin(['JobDirectory','JobFile','ShellScript'])], error_effects], axis=1)

tfs.write("error_scan_results.tfs",full_df, save_index="job_number")

exit()

fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(16,14))

ax[0].scatter(full_df['QUAD_FIELD_ERROR'],full_df['XRMSBB'], label="X") 
ax[0].scatter(full_df['QUAD_FIELD_ERROR'],full_df['YRMSBB'], label="Y") 
ax[1].scatter(full_df['ARCDISPL'],full_df['XRMSBB'], label="X") 
ax[1].scatter(full_df['ARCDISPL'],full_df['YRMSBB'], label="Y") 

ax[0].legend()
ax[1].legend()

axislabelsize=15

ax[0].set_xlabel("Arc Quad Angle Error, rad", fontsize=axislabelsize)
ax[1].set_xlabel("Arc Quad Misalignment, m", fontsize=axislabelsize)
ax[0].set_ylabel("XRMSBB, %", fontsize=axislabelsize)
ax[1].set_ylabel("XRMSBB, %", fontsize=axislabelsize)

plt.show()

exit()


