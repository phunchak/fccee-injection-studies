import tfs
import matplotlib.pyplot as plt
import numpy as np
import pathlib as Path
from datetime import datetime
import pandas as pd
import sys

#error_twiss_df_filename = str(sys.argv)


#read in files
tapered_lattice_df = tfs.read("twiss_FCCee_z_tapered.tfs", index='NAME')
tapered_lattice_errors_df = tfs.read("twiss_FCCee_z_tapered_errors.tfs", index='NAME')

#calculate maximum difference in orbit
x_max_orbit_diff = max(tapered_lattice_errors_df.X)
y_max_orbit_diff = max(tapered_lattice_errors_df.Y)

#select only QF magnets for RMS beta beating
tapered_lattice_df_QF		= tapered_lattice_df.loc[tapered_lattice_df.index.str.contains("^QF[24]\..*")]
tapered_lattice_errors_df_QF	= tapered_lattice_errors_df.loc[tapered_lattice_errors_df.index.str.contains("^QF[24]\..*")]

#calculate beta beating at QF locations
x_beta_beating = (tapered_lattice_errors_df_QF.BETX - tapered_lattice_df_QF.BETX) * 100 / tapered_lattice_df_QF.BETX
y_beta_beating = (tapered_lattice_errors_df_QF.BETY - tapered_lattice_df_QF.BETY) * 100 / tapered_lattice_df_QF.BETY

#calculate RMS
rms_x_beta_beating = np.sqrt(np.mean(np.square(x_beta_beating)))
rms_y_beta_beating = np.sqrt(np.mean(np.square(y_beta_beating)))

#store values in dataframe
df = pd.DataFrame(data =	{'XRMSBB': [rms_x_beta_beating], 'YRMSBB': [rms_y_beta_beating],\
				'XMAXOD': [x_max_orbit_diff],   'YMAXOD': [y_max_orbit_diff],\
				'Q1': [tapered_lattice_errors_df.headers['Q1']],	'Q2': [tapered_lattice_errors_df.headers['Q2']],\
				'EX': [tapered_lattice_errors_df.headers['EX']],	'EY': [tapered_lattice_errors_df.headers['EY']]})


#tfs.write("Outputdata/error_effects.tfs", df)
tfs.write("error_effects.tfs", df)

exit()


