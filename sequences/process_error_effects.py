import tfs
import matplotlib.pyplot as plt
import numpy as np
import pathlib as Path
from datetime import datetime
import pandas as pd

#read in error effect dataframes
dataFolder 	= "lattice_errors"
Jobs		= tfs.read('./{}/Jobs.tfs'.format(dataFolder))
numjobs		= max(Jobs.index)+1

df_errors =  tfs.read('./{}/Job.{}/Outputdata/error_effects.tfs'.format(dataFolder,0))
df_errors['job_number'] = 0
df_errors.set_index('job_number')


for i in range(1,numjobs,1):
	df_new_row = tfs.read('./{}/Job.{}/Outputdata/error_effects.tfs'.format(dataFolder,i))
	df_new_row['job_number'] =  i
	df_new_row.set_index('job_number')
	df_errors = pd.concat([df_errors,df_new_row], ignore_index=True)

tfs.write("error_effects_comparison.tfs", df_errors)

exit()


