import tfs
import matplotlib.pyplot as plt

#read in files
a = tfs.read("twiss_FCCee_z_thin_tapered.tfs")
b = tfs.read("twiss_FCCee_z_thin_tapered_errors.tfs")

x_beta_beating = (b.BETX-a.BETX)*100/a.BETX
y_beta_beating = (b.BETY-a.BETY)*100/a.BETY


fig, ax = plt.subplots(nrows=4, ncols=1, figsize=(9,16),gridspec_kw={'height_ratios':[2,2,1,1]})
bbox_inches='tight'

ax[0].plot(a.S,x_beta_beating)
ax[0].set_ylabel("Horz. \u03B2 Beating (%)",fontsize=20)
ax[1].plot(a.S,y_beta_beating)
ax[1].set_ylabel("Vert. \u03B2 Beating (%)",fontsize=20)
ax[2].plot(a.S,b.X,label="x")
ax[3].plot(a.S,b.Y,label="y")
ax[2].set_ylabel("Orbit Diff. X (m)",fontsize=20)
ax[3].set_ylabel("Orbit Diff. Y (m)",fontsize=20)
ax[2].set_xlabel("s (m)",fontsize=20)
ax[0].tick_params(axis="both", labelsize=15)
ax[1].tick_params(axis="both", labelsize=15)
ax[2].tick_params(axis="both", labelsize=15)
ax[3].tick_params(axis="both", labelsize=15)

ax[2].set_ylim([-250e-6,250e-6])
ax[2].axhline(y=50e-6,color='red')
ax[2].axhline(y=-50e-6,color='red')
ax[3].set_ylim([-250e-6,250e-6])
ax[3].axhline(y=50e-6,color='red')
ax[3].axhline(y=-50e-6,color='red')

plt.savefig('beta_beating.png', bbox_inches='tight')
#plt.show()


exit()


