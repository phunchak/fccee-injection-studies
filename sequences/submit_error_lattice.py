import numpy as np
from pylhc_submitter.job_submitter import main as htcondor_submit

if __name__ == "__main__":
    htcondor_submit(
        executable="/afs/cern.ch/user/m/mad/bin/rel/5.07.00/madx-linux64-gnu",  # default pointing to the latest MAD-X on afs
        mask="/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/sequences/setup_z_lattice_with_errors.mask",  # template to fill and execute MAD-X on
        replace_dict=dict(  # parameters to look for and replace in the template file
            ARCDISPL		=np.linspace(0, 20e-7, 5, endpoint=True).tolist(),
            ARCANGLE		=np.linspace(0, 10e-7, 3, endpoint=True).tolist(),
	    BENDDISPL 		=np.linspace(0, 1e-9, 2, endpoint=True).tolist(),
	    BENDANGLE		=np.linspace(0, 0, 1, endpoint=True).tolist(),
	    QUAD_FIELD_ERROR 	=np.linspace(0,2,3, endpoint=False).tolist(), 
        ),
	working_directory = "/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/sequences/lattice_errors",
        jobflavour="espresso",  # htcondor flavour
        run_local = False,
	resume_jobs=True,
	check_files=["error_effects.tfs"],
	num_processes = 1,
	dryrun=False,
    )
