
!----------------------------------------------------------------------------------------
! Set up Lattice

SET, FORMAT = "19.15f";

CALL, FILE = "/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/sequences/FCCee_z_216_nosol_1.seq";


!----------------------------------------------------------------------------------------
! Define Beam
! found in fcc_ee_z.madx on https://gitlab.cern.ch/mihofer/fcc-ee-collimation-lattice/-/blob/master/tests/fcc_ee_z.madx

pbeam := 45.6;      !beam 
EXbeam = 0.27e-9;   !horizontal emittance
EYbeam = 1.0e-12;   !vertical emittance
Nbun := 16640;      !number of bunches
NPar := 1.7e11;     !number of particles per bunch

Ebeam := sqrt( pbeam^2 + emass^2 ); !energy of the beam


// Beam defined without radiation as a start - radiation is turned on later depending on the requirements
BEAM, PARTICLE=POSITRON, NPART=Npar, KBUNCH=Nbun, ENERGY=Ebeam, RADIATE=FALSE, BV=+1, EX=EXbeam, EY=EYbeam;

USE, SEQUENCE = L000015;
SAVEBETA, LABEL=B.RF1, PLACE=FF2.1, SEQUENCE=L000015;
SAVEBETA, LABEL=B.RF2, PLACE=FF2.3, SEQUENCE=L000015;
TWISS, TOLERANCE=1E-12; ! Twiss without radiation and tapering

!----------------------------------------------------------------------------------------
! Perform initial Twiss and survey - ideal machine with no radiation

SEQEDIT, SEQUENCE = L000015;
         FLATTEN;
         INSTALL, ELEMENT = INJSEPTMARK, CLASS = MARKER, AT = 5922.736613;
         FLATTEN;
ENDEDIT;

! Set up matrix element
! MAD-X initializes variables with zero whereas uninitialized parameters for matrix are set to identity transform
! Thus R values should be properly set before twiss is called
trombone1: matrix, rm11 := r11.1,
                  rm12 := r12.1,
                  rm16 := r16.1,
                  rm21 := r21.1,
                  rm22 := r22.1,
                  rm26 := r26.1,
                  rm33 := r33.1,
                  rm34 := r34.1,
                  rm43 := r43.1,
                  rm44 := r44.1,
                  rm51 := r51.1,
                  rm52 := r52.1,
                  rm55 := 1.,
                  rm66 := 1.,
                  rm36 := r36.1,
                  rm46 := r46.1,
                  rm53 := r53.1,
                  rm54 := r54.1;

trombone2: matrix, rm11 := r11.2,
                  rm12 := r12.2,
                  rm16 := r16.2,
                  rm21 := r21.2,
                  rm22 := r22.2,
                  rm26 := r26.2,
                  rm33 := r33.2,
                  rm34 := r34.2,
                  rm43 := r43.2,
                  rm44 := r44.2,
                  rm51 := r51.2,
                  rm52 := r52.2,
                  rm55 := 1.,
                  rm66 := 1.,
                  rm36 := r36.2,
                  rm46 := r46.2,
                  rm53 := r53.2,
                  rm54 := r54.2;

!Define desired tune change
dphix = 0.00;
dphiy = 0.00;

! Set trombone values to correct tune
r11.1 := cos(twopi * dphix) + B.RF1->ALFX * sin(twopi * dphix);
r12.1 := B.RF1->BETX * sin(twopi * dphix);
r22.1 := cos(twopi * dphix) - B.RF1->ALFX * sin(twopi * dphix);
r21.1 := -sin(twopi * dphix) * (1 + (B.RF1->ALFX)^2) / B.RF1->BETX;
r33.1 := cos(twopi * dphiy) + B.RF1->ALFY * sin(twopi * dphiy);
r34.1 := B.RF1->BETY * sin(twopi * dphiy);
r44.1 := cos(twopi * dphiy) - B.RF1->ALFY * sin(twopi * dphiy);
r43.1 := -sin(twopi * dphiy) * (1 + (B.RF1->ALFY)^2) / B.RF1->BETY;
r16.1 := B.RF1->DX * (1 - r11.1) - r12.1 * B.RF1->DPX;
r26.1 := B.RF1->DPX * (1 - r22.1) - r21.1 * B.RF1->DX;
r51.1 := r21.1 * r16.1 - r11.1 * r26.1;
r52.1 := r22.1 * r16.1 - r12.1 * r26.1;
! below r expressions not confirmed
r36.1 := B.RF1->DY * (1 - r33.1) - r34.1 * B.RF1->DPY;
r46.1 := B.RF1->DPY * (1 - r44.1) - r43.1 * B.RF1->DY;
r53.1 := r43.1 * r61.1 - r33.1 * r62.1;
r54.1 := r44.1 * r61.1 - r34.1 * r62.1;

! Set trombone values to correct tune
r11.2 := cos(twopi * dphix) + B.RF2->ALFX * sin(twopi * dphix);
r12.2 := B.RF2->BETX * sin(twopi * dphix);
r22.2 := cos(twopi * dphix) - B.RF2->ALFX * sin(twopi * dphix);
r21.2 := -sin(twopi * dphix) * (1 + (B.RF2->ALFX)^2) / B.RF2->BETX;
r33.2 := cos(twopi * dphiy) + B.RF2->ALFY * sin(twopi * dphiy);
r34.2 := B.RF2->BETY * sin(twopi * dphiy);
r44.2 := cos(twopi * dphiy) - B.RF2->ALFY * sin(twopi * dphiy);
r43.2 := -sin(twopi * dphiy) * (1 + (B.RF2->ALFY)^2) / B.RF2->BETY;
r16.2 := B.RF2->DX * (1 - r11.2) - r12.2 * B.RF2->DPX;
r26.2 := B.RF2->DPX * (1 - r22.2) - r21.2 * B.RF2->DX;
r51.2 := r21.2 * r16.2 - r11.2 * r26.2;
r52.2 := r22.2 * r16.2 - r12.2 * r26.2;
! below r expressions not confirmed
r36.2 := B.RF2->DY * (1 - r33.2) - r34.2 * B.RF2->DPY;
r46.2 := B.RF2->DPY * (1 - r44.2) - r43.2 * B.RF2->DY;
r53.2 := r43.2 * r61.2 - r33.2 * r62.2;
r54.2 := r44.2 * r61.2 - r34.2 * r62.2;


SEQEDIT, SEQUENCE = L000015;
         FLATTEN;
         INSTALL, ELEMENT = TROMBONE1, AT = 0, FROM = FF2.1;
         INSTALL, ELEMENT = TROMBONE2, AT = 0, FROM = FF2.3;
         FLATTEN;
ENDEDIT;

!Setup to Correct Chromaticity
SF_KNOB := 1;
SD_KNOB := 1;

add_sext_knob(Sext_Type, Sext_Num): MACRO = {
	ADD2EXPR, var = K2Sext_TypeSext_Num, expr=*Sext_Type_KNOB;
	SHOW, K2Sext_TypeSext_Num;
};

n=0;
WHILE (n <= 103) {
	n = n + 1;
	EXEC, add_sext_knob(SF,$n);
	EXEC, add_sext_knob(SD,$n);
};

SHOW, K2SF1;
VALUE, K2SF1;

USE, SEQUENCE = L000015;

!----------------------------------------------------------------------------------------
! Load Aperture definitions
CALL, FILE="/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/sequences/aperture/FCCee_aper_definitions.madx";

// Save the voltage settings for the cavities for later use if needed
VOLTCA1SAVE = VOLTCA1; 

SHOW, VOLTCA1SAVE;

// Turn off the cavities for ideal machine twiss and survey
VOLTCA1 = 0;

SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L,K0L,K1L,K2L,K3L,K4L,K1SL,K2SL,K3SL,K4SL,HKICK,VKICK,BETX,BETY,ALFX,ALFY,MUX,MUY,DX,DY,DPX,DPY,R11,R12,R22,R21,X,PX,Y,PY,T,PT,DELTAP,VOLT,LAG,HARMON,FREQ,E1,E2,APERTYPE,APER_1,APER_2,APER_3,APER_4,TILT,ANGLE;

// Place where the initial conditions are saved - used for RF matching later if needed
SAVEBETA, LABEL=B.IP, PLACE=#s, SEQUENCE=L000015;


TWISS, FILE = "twiss_FCCee_z_b1_nottapered.tfs", TOLERANCE=1E-12; ! Twiss without radiation and tapering



!---------------------------------------------------------------------------------------
!Turn on RF and Synchrotron Radiation

// RF back on
VOLTCA1 = VOLTCA1SAVE;

// Turn the beam radiation on. N.B. This simple toggle works only if the sequence is not defined in the orginal beam command.
BEAM, RADIATE=TRUE;

// RF matching
LAGCA1 = 0.5;

MATCH, sequence=L000015, BETA0 = B.IP, tapering;
  VARY, NAME=LAGCA1, step=1.0E-7;
  CONSTRAINT, SEQUENCE=L000015, RANGE=#e, PT=0.0;
  JACOBIAN,TOLERANCE=1.0E-14, CALLS=3000;
ENDMATCH;

// Twiss with tapering
USE, SEQUENCE = L000015;

SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L,K0L,K1L,K2L,K3L,K4L,K1SL,K2SL,K3SL,K4SL,HKICK,VKICK,BETX,BETY,ALFX,ALFY,MUX,MUY,DX,DY,DPX,DPY,R11,R12,R22,R21,X,PX,Y,PY,T,PT,DELTAP,VOLT,LAG,HARMON,FREQ,E1,E2,APERTYPE,APER_1,APER_2,APER_3,APER_4,TILT,ANGLE;

!---------------------------------------------------------------------------------------
!Twiss before misalignments are introduced
TWISS, TAPERING, file="twiss_FCCee_z_tapered.tfs";

QX_DESIGN = table(summ, Q1); 	!Tune
QY_DESIGN = table(summ, Q2);
DQX_DESIGN = table(summ, DQ1); 	!Chromaticity
DQY_DESIGN = table(summ, DQ2);

!---------------------------------------------------------------------------------------
! Error Definition
EOPTION, SEED = 1;

ARC_DISPLACEMENT_QUAD 	:= %(ARCDISPL)s;
ARC_ANGLE_QUAD	 	:= %(ARCANGLE)s;
BEND_DISPLACEMENT := %(BENDDISPL)s;
BEND_ANGLE 	  := %(BENDANGLE)s;
ARC_DISPLACEMENT_SEXT  	:= 0;
ARC_ANGLE_SEXT	  	:= 0;
Q_FIELD_ERROR	:= %(QUAD_FIELD_ERROR)s;


SELECT, FLAG = Error, CLEAR;
!Arc Sextupoles
SELECT, FLAG = Error, PATTERN = "^SD.*";
SELECT, FLAG = Error, PATTERN = "^SF.*";

EALIGN, DX := ARC_DISPLACEMENT_SEXT*TGAUSS(2.5), DY := ARC_DISPLACEMENT_SEXT*TGAUSS(2.5), DS := ARC_DISPLACEMENT_SEXT*TGAUSS(2.5), DPSI := ARC_ANGLE_SEXT*TGAUSS(2.5), DPHI := ARC_ANGLE_SEXT*TGAUSS(2.5), DTHETA := ARC_ANGLE_SEXT*TGAUSS(2.5);
!EALIGN, DX := ARC_DISPLACEMENT_SEXT*TGAUSS(2.5), DY := ARC_DISPLACEMENT_SEXT*TGAUSS(2.5), DS := ARC_DISPLACEMENT_SEXT*TGAUSS(2.5);


SELECT, FLAG = Error, CLEAR;
!Arc Quadrupoles
SELECT, FLAG = Error, PATTERN = "^QF.*";
SELECT, FLAG = Error, PATTERN = "^QD.*";

EALIGN, DX := ARC_DISPLACEMENT_QUAD*TGAUSS(2.5), DY := ARC_DISPLACEMENT_QUAD*TGAUSS(2.5), DS := ARC_DISPLACEMENT_QUAD*TGAUSS(2.5), DPSI := ARC_ANGLE_QUAD*TGAUSS(2.5), DPHI := ARC_ANGLE_QUAD*TGAUSS(2.5), DTHETA := ARC_ANGLE_QUAD*TGAUSS(2.5);
!EALIGN, DX := ARC_DISPLACEMENT_QUAD*TGAUSS(2.5), DY := ARC_DISPLACEMENT_QUAD*TGAUSS(2.5), DS := ARC_DISPLACEMENT_QUAD*TGAUSS(2.5);

!Field Error
EFCOMP, ORDER=1, RADIUS=0.01, DKNR := {0,Q_FIELD_ERROR*TGAUSS(2.5)};

SELECT, FLAG = Error, CLEAR;

!Dipoles
SELECT, FLAG = Error, PATTERN = "^B.*";
EALIGN, DX := BEND_DISPLACEMENT*TGAUSS(2.5), DY := BEND_DISPLACEMENT*TGAUSS(2.5), DS := BEND_DISPLACEMENT*TGAUSS(2.5), DPSI := BEND_ANGLE*TGAUSS(2.5), DPHI := 0, DTHETA := 0;

SELECT, FLAG = Error, FULL;
ESAVE, FILE = "Outputdata/err2.file";

!---------------------------------------------------------------------------------------
!perform emittance and twiss calculations after misalignments have been included
EMIT, DELTAP=0;

VALUE, B.RF1->BETX;

SAVEBETA, LABEL=BERRORS, PLACE=#s, SEQUENCE=L000015;
SAVEBETA, LABEL=B.RF1.2, PLACE=FF2.1, SEQUENCE=L000015;
SAVEBETA, LABEL=B.RF2.2, PLACE=FF2.3, SEQUENCE=L000015;

VALUE, B.RF1->BETX; !At this point it is evaluated as zero, SAVEBETA clears it
		    !This sets certain trombone matrix elements to zero which is problematic
		    !and can alter the lattice. Switched savebeta to a different name.

TWISS, FILE = "twiss_FCCee_z_tapered_errors_before_tune_corr.tfs", TOLERANCE=1E-12; ! Twiss without radiation and tapering, errors included

VALUE, B.RF1->BETX;

!Transfer B.RF?.2 to B.RF?
B.RF1->BETX 	=  B.RF1.2->BETX;
B.RF1->BETY 	=  B.RF1.2->BETY;
B.RF1->ALFX 	=  B.RF1.2->ALFX;
B.RF1->ALFY 	=  B.RF1.2->ALFY;
B.RF1->DX 	=  B.RF1.2->DX;
B.RF1->DY 	=  B.RF1.2->DY;
B.RF1->DPX 	=  B.RF1.2->DPX;
B.RF1->DPY 	=  B.RF1.2->DPY;

B.RF2->BETX 	=  B.RF2.2->BETX;
B.RF2->BETY 	=  B.RF2.2->BETY;
B.RF2->ALFX 	=  B.RF2.2->ALFX;
B.RF2->ALFY 	=  B.RF2.2->ALFY;
B.RF2->DX 	=  B.RF2.2->DX;
B.RF2->DY 	=  B.RF2.2->DY;
B.RF2->DPX 	=  B.RF2.2->DPX;
B.RF2->DPY 	=  B.RF2.2->DPY;
!--------------------------------------------------


QX_ERRORS = table(summ, Q1);
QY_ERRORS = table(summ, Q2);
DQX_ERRORS = table(summ, DQ1);
DQY_ERRORS = table(summ, DQ2);

VALUE, QX_ERRORS;
VALUE, QY_ERRORS;
VALUE, DQX_ERRORS;
VALUE, DQY_ERRORS;

!Define desired tune change (divide by 2 as correcting with two phase trombones for symmetry)
dphix = (QX_DESIGN - QX_ERRORS)/2;
dphiy = (QY_DESIGN - QY_ERRORS)/2;

!---------------------------------------------------------------------------------------
!Correct Chromaticity

MATCH, sequence=L000015;
	VARY, NAME = SF_KNOB, STEP = 1e-07;
	VARY, NAME = SD_KNOB, STEP = 1e-07;
	!GLOBAL, DQ1 = DQX_DESIGN, DQ2 = DQY_DESIGN;
	GLOBAL, DQ1 = 0, DQ2 = 0;
	LMDIF, 	TOLERANCE=1.0E-14, CALLS=3000;
ENDMATCH;


TWISS, FILE = "twiss_FCCee_z_tapered_errors.tfs", TOLERANCE=1E-12; ! Twiss without radiation and tapering, errors included

VALUE, table(summ, DQ1);

!Call python script to create tfs of parameters

SYSTEM, "/afs/cern.ch/user/p/phunchak/work/public/submitterpython/bin/python /afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/sequences/rms_beta_beating.py";

STOP;
