import numpy as np
from pylhc_submitter.job_submitter import main as htcondor_submit

if __name__ == "__main__":
    htcondor_submit(
        executable="../../codes/madx",  # default pointing to the latest MAD-X on afs
        mask="../scripts/FCCee_z_Dynap.mask",  # template to fill and execute MAD-X on
        replace_dict=dict(  # parameters to look for and replace in the template file
            XCOORD = [1,2],
            YCOORD = [1,2],
        ),
	working_directory = "./",
        jobflavour="tomorrow",  # htcondor flavour
        run_local = True,
	dryrun=False,
    )
