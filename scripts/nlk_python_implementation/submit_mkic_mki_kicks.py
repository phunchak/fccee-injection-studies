import numpy as np
from pylhc_submitter.job_submitter import main as htcondor_submit

if __name__ == "__main__":
    htcondor_submit(
        executable="/afs/cern.ch/user/m/mad/bin/rel/5.07.00/madx-linux64-gnu",
        mask="/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/scripts/nlk_python_implementation/Track_MKI_and_MKIC.mask",  # template to fill and execute MAD-X on
        replace_dict=dict(  # parameters to look for and replace in the template file
	    MKIC_MISALIGN = [0, 0.0001, 0.0002],
	    MKI_MISALIGN  = [0, 0.0001, 0.0002],
        ),
	working_directory = "/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/scripts/nlk_python_implementation/Track_MKI_Comp_Condor",
        jobflavour="testmatch",  # htcondor flavour
        run_local = False,
	resume_jobs=False,
	dryrun=False,
    )

