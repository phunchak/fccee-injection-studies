!------------------------------------------------------------------------
! Setup lattice
!------------------------------------------------------------------------
EOPTION, SEED=2;
!MKI_INCLUDE = 1; !set so that install occurs in setup_z_lattice_with_errors.madx
CALL, FILE = "/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/scripts/test_nlk_python_implementation/full_setup_z_lattice.madx";
EOPTION, SEED=2;

!setup_z_lattice_with_errors.madx cycles to the MKI when MKI_INCLUDE =1
SELECT, FLAG = TWISS, CLEAR;
SELECT, FLAG = TWISS, PATTERN = "MKI_MARKER";
SELECT, FLAG = TWISS, PATTERN = "MKIC_MARKER";
SELECT, FLAG = TWISS, PATTERN = "RECTCOLLIMATOR";
SELECT, FLAG = TWISS, PATTERN = "IP.1";
SELECT, FLAG = TWISS, PATTERN = "INJSEPTMARK";
TWISS, FILE = "twiss_key_points.tfs";
SELECT, FLAG = TWISS, CLEAR;

!TWISS, FILE = "twiss_mki_err.tfs";
TWISS;

xstart = table(twiss,ip.1,x);
ystart = table(twiss,ip.1,y);
pxstart = table(twiss,ip.1,px);
pystart = table(twiss,ip.1,py);
tstart = table(twiss,ip.1,t);
ptstart = table(twiss,ip.1,pt);
BETXSTART = table(twiss,ip.1,betx);
BETYSTART = table(twiss,ip.1,bety);
ALFXSTART = table(twiss,ip.1,alfx);
ALFYSTART = table(twiss,ip.1,alfy);

value,xstart;
value,ystart;
value,pxstart;
value,pystart;
value,tstart;
value,ptstart;

value,betxstart;
value,betystart;
value,alfxstart;
value,alfystart;

!--------------------------------------------------------------------------
! Define track settings, control of injection magnets.
!------------------------------------------------------------------------
!kick_factor = -7.9e-3;
!kick_factor = 0.007612362;
!injection_sigma_offset = 23; !set to zero for stored beam tracking
!injection_phase_offset = 20; 
injection_sigma_offset = 0; !set to zero for stored beam tracking
injection_phase_offset = 0; 


tracking_turns = 10;
number_of_particles = 10000;

!--------------------------------------------------------------------------
! Tracking of gaussian beam in horizontal phase space
!--------------------------------------------------------------------------

OPTION, WARN=false;

TRACK,  ONETABLE=TRUE, 
        FILE="Outputdata/track_IP_to_MKIC.tfs", 
        DAMP=TRUE,
        RECLOSS = TRUE,
        UPDATE = FALSE,
        APERTURE = TRUE,
        QUANTUM=TRUE; !need this on for emittance tracking


enomphysx = EXBeam;
enomphysy = EYBeam;

myfx=injection_sigma_offset*sqrt(enomphysx)*cos(injection_phase_offset*PI/180);
myfpx=injection_sigma_offset*sqrt(enomphysx)*sin(injection_phase_offset*PI/180);

!injection offset of tracked beam is zero by default
inj_x = 0;
inj_px = 0;

!inj_x = myfx*sqrt(BETXSTART);
!inj_px= -1*(ALFXSTART)*myfx / SQRT(BETXSTART) + myfpx / sqrt(BETXSTART);

CREATE, TABLE = "initial_coordinates", COLUMN = NUMBER,X,PX,Y,PY;

! Loop initializing particles
n = 1;
while (n <= number_of_particles) {
	Nsigmax = tgauss(5);
	Nsigmay = tgauss(5);
	Nsigmapx = tgauss(5);
	Nsigmapy = tgauss(5);
	phasex = RANF() * 2 * PI;
	phasey = RANF() * 2 * PI;

	! Defines the used emittance
	enomphysx = EXBeam;	!injected beam emittance
	enomphysy = EYBeam;
	myx = sqrt(BETXSTART) * Nsigmax * sqrt(enomphysx);
	myy = sqrt(BETYSTART) * Nsigmay * sqrt(enomphysy);
	mypx= -1 * Nsigmax * sqrt(enomphysx) * (ALFXSTART) / SQRT(BETXSTART) + Nsigmapx *sqrt(enomphysx) / sqrt(BETXSTART);
	mypy= -1 * Nsigmay * sqrt(enomphysy) * (ALFYSTART) / SQRT(BETYSTART) + Nsigmapy *sqrt(enomphysy) / sqrt(BETYSTART);
	
	START, X = xstart + (inj_x) + (myx), PX = pxstart + (inj_px) + (mypx), Y = ystart + (myy), PY = pystart + (mypy), T = tstart, PT = ptstart;
	
	n = n + 1;
	};

OBSERVE, PLACE=IP.1;
OBSERVE, PLACE=MKI_MARKER;
OBSERVE, PLACE=MKIC_MARKER;
RUN, TURNS=1;
!Tracking complete
ENDTRACK; 
WRITE, TABLE = trackloss, FILE = "Outputdata/loss_IP_to_MKIC.tfs";

!--------------------------------------------------------------------------
!process MKIC kick
!--------------------------------------------------------------------------
SYSTEM, "/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/xsuite/xpython/bin/python /afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/scripts/test_nlk_python_implementation/call_mkic_kick.py %(MKIC_MISALIGN)s";
!cycle to start tracking from MKIC
SEQEDIT, SEQUENCE = L000015;
	FLATTEN;
	CYCLE, START = MKIC_MARKER;
	FLATTEN;
ENDEDIT;
USE, SEQUENCE = L000015;
!TWISS, FILE = "test_twiss.tfs";
!--------------------------------------------------------------------------
!track from MKIC to MKI
!--------------------------------------------------------------------------
TRACK,  ONETABLE=TRUE, 
        FILE="Outputdata/track_MKIC_to_MKI.tfs", 
        DAMP=TRUE,
        RECLOSS = TRUE,
        UPDATE = FALSE,
        APERTURE = TRUE,
        QUANTUM=TRUE; !need this on for emittance tracking

!CALL FILE WITH OUTPUT OF PYTHON APPLICATION OF KICK TO GET INITIALIZATION OF PARTICLES
CALL, FILE="kicked_beam_post_mkic.madx";

OBSERVE, PLACE=MKI_MARKER;
OBSERVE, PLACE=IP.1;
RUN, TURNS=1;
!Tracking complete
ENDTRACK;
WRITE, TABLE = trackloss, FILE = "Outputdata/loss_MKIC_to_MKI.tfs";

!--------------------------------------------------------------------------
!process MKI kick
!--------------------------------------------------------------------------
SYSTEM, "/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/xsuite/xpython/bin/python /afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/scripts/test_nlk_python_implementation/call_mki_kick.py %(MKI_MISALIGN)s";
!cycle to start tracking from MKI
SEQEDIT, SEQUENCE = L000015;
	FLATTEN;
	CYCLE, START = MKI_MARKER;
	FLATTEN;
ENDEDIT;
USE, SEQUENCE = L000015;
!--------------------------------------------------------------------------
!track from MKI onward
!--------------------------------------------------------------------------
TRACK,  ONETABLE=TRUE, 
        FILE="Outputdata/track_MKI_onward.tfs", 
        DAMP=TRUE,
        RECLOSS = TRUE,
        UPDATE = FALSE,
        APERTURE = TRUE,
        QUANTUM=TRUE; !need this on for emittance tracking

!CALL FILE WITH OUTPUT OF PYTHON APPLICATION OF KICK TO GET INITIALIZATION OF PARTICLES
CALL, FILE="kicked_beam_post_mki.madx";

OBSERVE, PLACE=IP.1;
RUN, TURNS=tracking_turns;
!Tracking complete
ENDTRACK;
WRITE, TABLE = trackloss, FILE = "Outputdata/loss_MKI_onward.tfs";

!--------------------------------------------------------------------------
! EOF
