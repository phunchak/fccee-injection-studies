import numpy as np
from nlk_tracking import main as apply_mki_kick
import sys

# check number of inputs
if len(sys.argv) ==  2:
	horiz_misalign = sys.argv[1]
	print("MKI misalignment set to: {} m".format(horiz_misalign))
else:
	horiz_misalign = 0.00
	print("MKI misalignment set to: {} m".format(horiz_misalign))

if __name__ == "__main__":
	apply_mki_kick(
	#trackone_file 	= "/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/scripts/nlk_python_implementation/track_MKIC_to_MKI.tfsone",
	trackone_file 	= "Outputdata/track_MKIC_to_MKI.tfsone",
	kickprofile_file= "/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/kicker_data/KickerFieldProfile.csv",
	kickstrength	= (0.5/152.105227)*(-0.0079), #0.5/beam_rigidity * kick_factor
	injection_point	= 0.00575,
	madx_file	= "kicked_beam_post_mki.madx",
	df_file		= "Outputdata/coord_df_post_mki.tfs",
	element		= "mki_marker",
	turn		= -1,
	nlk_offset	= float(horiz_misalign),
	)
