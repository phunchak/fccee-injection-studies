!----------------------------------------------------------------------
! Setup Lattice
!----------------------------------------------------------------------
CALL, FILE = "../sequences/setup_z_lattice.madx";

! cycle to track DA starting from injection septum
SEQEDIT, SEQUENCE = L000015;
         FLATTEN;
         CYCLE, START = INJSEPTMARK;
         FLATTEN;
ENDEDIT;

// Tracking
USE, SEQUENCE = L000015;
       
TRACK,  ONETABLE=FALSE, 
        FILE="track.tfs", 
        DAMP=TRUE,
        RECLOSS = TRUE,
        APERTURE = TRUE,
        QUANTUM=FALSE; !need this on for emittance tracking

! XCOORD and YCOORD are flags to be replaced for each initial position, see pyhcsubmitter documentation for potential use
START,  X = %(XCOORD)s, PX = 0, Y = %(YCOORD)s, PY = 0, T = 0, PT = 0;

RUN, TURNS=2500;
ENDTRACK;

!--------------------------------------------------------------------------
! Output
!--------------------------------------------------------------------------
WRITE, TABLE = trackloss, FILE = "loss.tfs";

