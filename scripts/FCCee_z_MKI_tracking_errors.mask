!------------------------------------------------------------------------
! Setup lattice
!------------------------------------------------------------------------
EOPTION, SEED=%(ERRORSEED)s;
MKI_INCLUDE = 1; !set so that install occurs in setup_z_lattice_with_errors.madx
CALL, FILE = "/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/sequences/setup_z_lattice_with_errors.madx";
EOPTION, SEED=%(PARTICLESEED)s;

!setup_z_lattice_with_errors.madx cycles to the MKI when MKI_INCLUDE =1

!TWISS, FILE = "twiss_mki_err.tfs";
TWISS;

xstart = table(twiss,mkikick.1,x);
ystart = table(twiss,mkikick.1,y);
pxstart = table(twiss,mkikick.1,px);
pystart = table(twiss,mkikick.1,py);
tstart = table(twiss,mkikick.1,t);
ptstart = table(twiss,mkikick.1,pt);
BETXSTART = table(twiss,mkikick.1,betx);
BETYSTART = table(twiss,mkikick.1,bety);
ALFXSTART = table(twiss,mkikick.1,alfx);
ALFYSTART = table(twiss,mkikick.1,alfy);

value,xstart;
value,ystart;
value,pxstart;
value,pystart;
value,tstart;
value,ptstart;

value,betxstart;
value,betystart;
value,alfxstart;
value,alfystart;


!--------------------------------------------------------------------------
! Define track settings, control of injection magnets.
!--------------------------------------------------------------------------
!kick_factor = -7.9e-3;
!kick_factor = 0.007612362;
!injection_sigma_offset = 23; !set to zero for stored beam tracking
!injection_phase_offset = 20; 
kick_factor = 0; ! set to zero for starting at MKI with zero angle.
on_injected_setting = 1;     !set to 0 for stored, 1 for injected beam kick
injection_sigma_offset = 10; !set to zero for stored beam tracking
injection_phase_offset = 0; 

on_stored_setting = 0;       !set to 1 for stored beam kick, 0 for injected beam 
on_comp_setting = 0;	     !set to 1 to turn on compensation kicker, 0 to turn it off

tracking_turns = 2500;
number_of_particles = 250;

tr$macro(TURN): macro={
if (TURN == 1){
    on_stored_beam = on_stored_setting;
    on_injected_beam = on_injected_setting;};
else{
    on_stored_beam = 0;
    on_injected_beam = 0;};
};


!--------------------------------------------------------------------------
! Tracking of gaussian beam in horizontal phase space
!--------------------------------------------------------------------------

OPTION, WARN=false;

TRACK,  ONETABLE=TRUE, 
        FILE="track_MKI_errors.tfs", 
        DAMP=TRUE,
        RECLOSS = TRUE,
        UPDATE = TRUE,
        APERTURE = TRUE,
        QUANTUM=TRUE; !need this on for emittance tracking


enomphysx = EXBeam;
enomphysy = EYBeam;

myfx=injection_sigma_offset*sqrt(enomphysx)*cos(injection_phase_offset*PI/180);
myfpx=injection_sigma_offset*sqrt(enomphysx)*sin(injection_phase_offset*PI/180);

!injection offset of tracked beam is zero by default
inj_x = 0;
inj_px = 0;

!if on_injected_setting is true (ie. tracking injected beam) then include injection offset
if (on_injected_setting == 1){	
	inj_x = myfx*sqrt(BETXSTART);
	!inj_px= -1*(ALFXSTART)*myfx / SQRT(BETXSTART) + myfpx / sqrt(BETXSTART);
	inj_px=0 !set for MKI to have bent the beam parallel to the design orbit
};

CREATE, TABLE = "initial_coordinates", COLUMN = NUMBER,X,PX,Y,PY;

! Loop initializing particles
n = 1;
while (n <= number_of_particles) {
	Nsigmax = tgauss(5);
	Nsigmay = tgauss(5);
	Nsigmapx = tgauss(5);
	Nsigmapy = tgauss(5);
	phasex = RANF() * 2 * PI;
	phasey = RANF() * 2 * PI;

	! Defines the used emittance
	enomphysx = EXBeam;	!injected beam emittance
	enomphysy = EYBeam;
	myx = sqrt(BETXSTART) * Nsigmax * sqrt(enomphysx);
	myy = sqrt(BETYSTART) * Nsigmay * sqrt(enomphysy);
	mypx= -1 * Nsigmax * sqrt(enomphysx) * (ALFXSTART) / SQRT(BETXSTART) + Nsigmapx *sqrt(enomphysx) / sqrt(BETXSTART);
	mypy= -1 * Nsigmay * sqrt(enomphysy) * (ALFYSTART) / SQRT(BETYSTART) + Nsigmapy *sqrt(enomphysy) / sqrt(BETYSTART);
	
	NUMBER = n;
	X  = inj_x+myx;
	PX = inj_px+mypx;
	Y  = myy;
	PY = mypy;
	FILL, TABLE = "initial_coordinates", ROW = n;
	n=n+1;
	!all initial conditions set to match error lattice closed orbit, each individual particle has injection offset and its individual position within the distribution
	!vertical emittance not included as the MKI kickers are only properly defined in the horizontal plane. (Inaccurate multipole effects with vertical offset)
	START, X = xstart + (inj_x) + (myx), PX = pxstart + (inj_px) + (mypx), Y = (ystart) + (myy), PY = (pystart) + (mypy), T = tstart, PT = ptstart;
	};

OBSERVE, PLACE=IP.1;
OBSERVE, PLACE=BEFOREKICKER;
OBSERVE, PLACE=AFTERKICKER;
RUN, TURNS=tracking_turns;
ENDTRACK;

WRITE, TABLE = initial_coordinates, FILE = "Outputdata/initial_coordinates.tfs";
WRITE, TABLE = trackloss, FILE = "Outputdata/loss_MKI.tfs";
!--------------------------------------------------------------------------
! EOF
