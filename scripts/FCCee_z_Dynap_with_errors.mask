!----------------------------------------------------------------------
! Setup Lattice
!----------------------------------------------------------------------
CALL, FILE = "/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/sequences/setup_z_lattice_with_errors.madx";

!setup_z_lattice_with_errors.madx cycles to the injection septum

// Tracking
TWISS;
xstart = table(twiss,injseptmark,x);
ystart = table(twiss,injseptmark,y);
pxstart = table(twiss,injseptmark,px);
pystart = table(twiss,injseptmark,py);
tstart = table(twiss,injseptmark,t);
ptstart = table(twiss,injseptmark,pt);


TRACK,  ONETABLE=FALSE, 
        FILE="track.tfs", 
        DAMP=TRUE,
        RECLOSS = TRUE,
        APERTURE = TRUE,
        QUANTUM=FALSE; !need this on for emittance tracking

!XCOORD and YCOORD are flags to be replaced for each initial position, see pyhcsubmitter documentation for potential use
START,  X = xstart + (%(XCOORD)s), PX = pxstart, Y = ystart + (%(YCOORD)s), PY = pystart, T = tstart, PT = ptstart;

RUN, TURNS=2500;
ENDTRACK;

!--------------------------------------------------------------------------
! Output
!--------------------------------------------------------------------------
WRITE, TABLE = trackloss, FILE = "Outputdata/loss.tfs";

!START,  X = xstart + 0, PX = pxstart, Y = ystart + 0, PY = pystart, T = tstart, PT = ptstart;
!START,  X = 0, PX = 0, Y = 0, PY = 0, T = 0, PT = 0;
