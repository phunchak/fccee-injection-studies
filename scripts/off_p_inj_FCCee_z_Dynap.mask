!----------------------------------------------------------------------
! Setup Lattice
!----------------------------------------------------------------------
!CALL, FILE = "../sequences/setup_z_off_p_lattice.madx";
CALL, FILE = "/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/sequences/setup_z_off_p_lattice.madx";


! cycle to track DA starting from injection septum
SEQEDIT, SEQUENCE = L000015;
         FLATTEN;
         CYCLE, START = INJSEPTMARK;
         FLATTEN;
ENDEDIT;

// Tracking
USE, SEQUENCE = L000015;

! injection momentum offset (PT = deltaE/pc, pg. 17 in MAD-X user's guide)
pt_inj 		= 0.01 ;
![percent] OBSOLETE IN DELTAP APPROACH
DX_inj	= PB.STRAIGHT->DX; 	
![m], dispersion at the injection septum determined by optics in setup_z_off_p_lattice
       
TRACK,  ONETABLE=FALSE,
	ONEPASS=FALSE, !set so that DELTAP functions properly (particle coordinates are in reference to off-p orbit)
	DELTAP=0.01,   !all particles initialized with 0.01 momentum offset 
        FILE="track.tfs", 
        DAMP=TRUE,
        RECLOSS = TRUE,
        APERTURE = TRUE,
        QUANTUM=FALSE; !need this on for emittance tracking

! XCOORD and YCOORD are flags to be replaced for each initial position, see pyhcsubmitter documentation for potential use
! transverse coordinates are shifted by dispersive offset at the injection septum so that x=y=0 is on-axis for the off-momentum orbit
!START,  X = %(XCOORD)s+pt_inj*DX_inj, PX = 0, Y = %(YCOORD)s, PY = 0, T = 0, PT = pt_inj;
START,  X = %(XCOORD)s, PX = 0, Y = %(YCOORD)s, PY = 0, T = 0, PT = 0;

RUN, TURNS=2500;
ENDTRACK;

!--------------------------------------------------------------------------
! Output
!--------------------------------------------------------------------------
WRITE, TABLE = trackloss, FILE = "Outputdata/loss.tfs";

