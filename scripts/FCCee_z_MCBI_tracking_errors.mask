!------------------------------------------------------------------------
! Setup lattice
!------------------------------------------------------------------------
EOPTION, SEED=%(ERRORSEED)s; ! seed that changes for varied generation of error distribution
MCBI_INCLUDE = 1;
CALL, FILE = "/afs/cern.ch/user/p/phunchak/work/public/fccee-injection-studies/sequences/setup_z_lattice_with_errors.madx";
EOPTION, SEED=%(PARTICLESEED)s; ! seed that changes for varied generation of particle distribution

TWISS;

betxstart = table(twiss, injseptmark, betx);
betystart = table(twiss, injseptmark, bety);
alfxstart = table(twiss, injseptmark, alfx);
alfystart = table(twiss, injseptmark, alfy);

VALUE, betxstart;
VALUE, betystart;
VALUE, alfxstart;
VALUE, alfystart;

!--------------------------------------------------------------------------
! Define track settings, control of injection magnets.
!--------------------------------------------------------------------------
!septum_width = 3e-3; 	!Eddy current septum
septum_width = 0.1e-3; !Electrostatic septum



bump_height_setting = 10*sqrt(EXBEAM*BETXSTART)+septum_width;
injection_sigma_offset = 23; !set to zero for stored beam tracking
injection_phase_offset = 0; 

tracking_turns = 2500;
number_of_particles = 250;

tr$macro(TURN): macro={
if (TURN == 1){
    bump_height = bump_height_setting;}
else{
    bump_height = 0;};
};


!--------------------------------------------------------------------------
! Tracking of gaussian beam in horizontal phase space
!--------------------------------------------------------------------------

OPTION, WARN=false;

TRACK,  ONETABLE=TRUE, 
    	ONEPASS=TRUE, !done so that particle initialization is not in reference to bumped orbit
        FILE="track_mcbi_errors.tfs", 
        DAMP=TRUE,
        RECLOSS = TRUE,
        UPDATE = TRUE,
        APERTURE = TRUE,
        QUANTUM=TRUE; !need this on for emittance tracking


enomphysx = EXBeam;
enomphysy = EYBeam;

myfx=injection_sigma_offset*sqrt(enomphysx)*cos(injection_phase_offset*PI/180);
myfpx=injection_sigma_offset*sqrt(enomphysx)*sin(injection_phase_offset*PI/180);

inj_x = bump_height_setting+5*sqrt(EXBEAM*BETXSTART)+septum_width+5*sqrt(EXBEAM*BETXSTART);
inj_px = 0;
!inj_px= -1*(ALFXSTART)*myfx / SQRT(BETXSTART) + myfpx / sqrt(BETXSTART);


CREATE, TABLE = "initial_coordinates", COLUMN = NUMBER,X,PX,Y,PY;

! Loop initializing particles
n = 1;
while (n <= number_of_particles) {
	Nsigmax = tgauss(5);
	Nsigmay = tgauss(5);
	Nsigmapx = tgauss(5);
	Nsigmapy = tgauss(5);
	phasex = RANF() * 2 * PI;
	phasey = RANF() * 2 * PI;

	! Defines the used emittance
	enomphysx = EXBeam;	!injected beam emittance
	enomphysy = EYBeam;
	myx = sqrt(BETXSTART) * Nsigmax * sqrt(enomphysx);
	myy = sqrt(BETYSTART) * Nsigmay * sqrt(enomphysy);
	mypx= -1 * Nsigmax * sqrt(enomphysx) * (ALFXSTART) / SQRT(BETXSTART) + Nsigmapx *sqrt(enomphysx) / sqrt(BETXSTART);
	mypy= -1 * Nsigmay * sqrt(enomphysy) * (ALFYSTART) / SQRT(BETYSTART) + Nsigmapy *sqrt(enomphysy) / sqrt(BETYSTART);
	
	NUMBER = n;
	X  = inj_x+myx;
	PX = inj_px+mypx;
	Y  = myy;
	PY = mypy;
	FILL, TABLE = "initial_coordinates", ROW = n;
	n=n+1;

	START, x=(inj_x +myx), px=(inj_px + mypx), y=myy, py=mypy;
};

OBSERVE, PLACE=RECTCOLLIMATOR;
RUN, TURNS=tracking_turns;
ENDTRACK;

WRITE, TABLE = initial_coordinates, FILE = "Outputdata/initial_coordinates.tfs";
WRITE, TABLE = trackloss, FILE = "Outputdata/loss_MCBI.tfs";
!--------------------------------------------------------------------------
! EOF
